import React from "react";
import {
  StyleSheet,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  FlatList,
  ActivityIndicator
} from "react-native";
import { Text, Button, Container } from "native-base";
import Icon from "react-native-vector-icons/Feather";
import { connect } from "react-redux";
import { Colors, formatCurrency } from "../../utils/const";
import FastImage from "react-native-fast-image";
import Modal from "react-native-modal";

class NewSaleProductDetails extends React.Component {
  constructor() {
    super();
    this.state = {
      showCostumer: false,
      isModalVisible: false,
      showModalLoadingDelete: false
    };
  }
  _toggleModal = () => {
    this.setState({ showCostumer: !this.state.showCostumer });
    setTimeout(() => this.setState({ showModal: false }), 3000);
  };
  // capitalize(str) {
  //   return str.charAt(0).toUpperCase() + str.slice(1);
  // }

  fitImage(image) {
    let fitImage = image.split("/upload/");
    return (
      "https://res.cloudinary.com/komercia-store/image/upload/w_250,q_auto:best,f_auto/" +
      fitImage[1]
    );
  }

  resetAllCart = () => {
    this.setState({ isModalVisible: true });
  };

  deleteOneElement = item => {
    console.warn(item);
    // this.props.removeFromCart(item);
  };

  modal() {
    const { navigate } = this.props.navigation;
    if (this.state.showModalLoadingDelete) {
      return (
        <View style={styles.modalContent}>
          <View
            style={{
              paddingTop: 5,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <ActivityIndicator size="large" color="#f14b5a" />
            <Text>Eliminando información</Text>
            <Text>Espere un momento por favor</Text>
          </View>
        </View>
      );
    } else {
      return (
        <View style={styles.modalContent}>
          <Text style={styles.modalText}>
            ¿Estas seguro que deseas elminar esta venta?
          </Text>
          <View
            style={{
              padding: 20,
              flexDirection: "row",
              alignItems: "center",
              alignContent: "center",
              justifyContent: "center"
            }}
          >
            <TouchableOpacity
              onPress={() => {
                this.props.resetAllCart(),
                  this.setState({ isModalVisible: false });
              }}
              style={styles.finishButton}
            >
              <Text style={{ color: "white" }}>Si</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.setState({ isModalVisible: false });
              }}
              style={styles.finishButton}
            >
              <Text style={{ color: "white" }}>No</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    }
  }

  subtotal = () => {
    let suma = Object.values(this.props.cartItems).reduce(
      (a, b) => a.precio + b.precio
    );
    return suma;
    // this.props.cartItems
  };

  addCostumer() {
    if (!this.state.showCostumer) {
      return (
        <TouchableOpacity
          style={styles.addCostumer}
          onPress={this._toggleModal}
        >
          <Icon name="plus-circle" style={styles.icon} />
          <Text>Agregar Cliente</Text>
        </TouchableOpacity>
      );
    } else {
      return (
        <View style={styles.costumerDetails}>
          <View>
            <Text style={styles.title}>Comprador</Text>
            <Text style={styles.nameProduct}>Nombre</Text>
            <Text>Juan Camilo Gonzalez</Text>
            <Text style={styles.nameProduct}>Email</Text>
            <Text>juancamilo@gmail.com</Text>
            <Text style={styles.nameProduct}>Telefono</Text>
            <Text>3124539289</Text>
          </View>
          <View style={styles.detailsLeft}>
            <Image
              style={styles.imageProfile}
              source={require("../../src/assets/profile.png")}
            />
            <Text style={styles.textPhotoProfile}>Comprador </Text>
          </View>
        </View>
      );
    }
  }

  render() {
    const { goBack } = this.props.navigation;
    const { navigate } = this.props.navigation;
    if (!this.props.cartItems.length) {
      return (
        <View
          style={{
            flex: 1,
            backgroundColor: "white",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <Image
            source={require("../../src/assets/nocart.png")}
            style={{ width: 200, height: 200 }}
          />
          <Text
            style={{
              color: "#4a4a4a",
              fontSize: 18,
              fontWeight: "700",
              marginBottom: 5
            }}
          >
            Tu carrito esta vacío
          </Text>
          <Text
            style={{
              color: "#4a4a4a",
              fontSize: 16,
              fontWeight: "400",
              width: 250,
              textAlign: "center"
            }}
          >
            Agrega productos de tu inventario para realizar una venta.
          </Text>
          <TouchableOpacity
            onPress={() => navigate("ListProducts")}
            style={{
              backgroundColor: Colors.first,
              marginTop: 25,
              padding: 12,
              elevation: 2,
              width: 170,
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Text style={{ color: "white" }}> Inventario</Text>
          </TouchableOpacity>
        </View>
      );
    } else {
      return (
        <Container>
          <Modal isVisible={this.state.isModalVisible}>{this.modal()}</Modal>
          <ScrollView>
            {/* <View style={styles.header}>
              <Button transparent onPress={() => goBack()}>
                <Icon name="arrow-left" color="black" size={30} />
              </Button>
              <TouchableOpacity style={styles.tabShare} onPress={() => {}}>
                <Icon name="trash-2" style={styles.tabCartIcon} />
              </TouchableOpacity>
            </View> */}

            <View style={styles.productDetails}>
              <Text style={styles.title}>
                Productos{" "}
                <Text style={{ fontSize: 13 }}>
                  ({this.props.cartItems.length})
                </Text>
              </Text>

              <FlatList
                style={styles.flatList}
                windowSize={20}
                removeClippedSubviews={true}
                data={this.props.cartItems}
                keyExtractor={(item, _) => item.nombre}
                renderItem={({ item }) => (
                  <View style={styles.itemList}>
                    <View style={styles.infoProduct}>
                      <FastImage
                        style={styles.imageProduct}
                        resizeMode={FastImage.resizeMode.contain}
                        source={{ uri: item.foto_cloudinary }}
                      />
                      <View style={styles.itemListText}>
                        <Text>{item.nombre}</Text>
                        <Text style={styles.textQuantity}>
                          $ {item.precio} X 8 unidades
                        </Text>
                      </View>
                      <View style={styles.itemListPrice}>
                        <Text style={styles.priceProduct}>
                          $ {formatCurrency(item.precio)}
                        </Text>
                      </View>
                      <View style={styles.itemListTrash}>
                        <TouchableOpacity
                          onPress={() => {
                            this.props.addItemToCart(item);
                          }}
                        >
                          <Icon name="trash-2" style={styles.iconTrash} />
                        </TouchableOpacity>
                      </View>
                    </View>
                    <View style={styles.separator} />
                  </View>
                )}
              />
            </View>

            {/* {this.addCostumer()} */}

            {/* <View style={styles.contentTotal}> */}
            {/* <View style={styles.sectionTotal}>
                <Text style={styles.textTotal}>Subtotal</Text>
                <Text style={styles.textTotal}>
                  $ {formatCurrency(this.subtotal())}
                </Text>
              </View> */}
            {/* <View style={styles.sectionTotal}>
                <Text style={styles.textTotal}>Impuestos</Text>
                <Text style={styles.textTotal}>$ 450.000</Text>
              </View> */}
            {/* <View style={styles.sectionTotal}>
                <Text style={styles.textTotal}>Descuentos</Text>
                <Text style={styles.textTotal}>$ 0</Text>
              </View> */}
            {/* <View
                style={{ width: "100%", height: 1, backgroundColor: "#4834d4" }}
              /> */}
            {/* </View> */}
          </ScrollView>

          <View style={styles.contentPriceTotal}>
            <View style={styles.sectionTotal}>
              <Text style={styles.textTotal}>Total Venta</Text>
              <Text style={styles.priceTotal}>
                $ {formatCurrency(this.subtotal())}
              </Text>
            </View>
          </View>

          <View style={styles.footer}>
            <TouchableOpacity
              style={[styles.buttonFooter, styles.buttonDeleteColor]}
              onPress={() => this.resetAllCart()}
            >
              <Icon name="trash-2" style={styles.icon} />
              {/* <Text style={{ fontSize: 11, color: "#4c4c4c", marginTop: 3 }}>
                BORRAR
              </Text> */}
            </TouchableOpacity>
            {/* <TouchableOpacity
              onPress={() => navigate("ListProducts")}
              style={[styles.buttonFooter, styles.buttonBackColor]}
            >
              <Text style={styles.textButton}>Seguir comprando</Text>
            </TouchableOpacity> */}
            <TouchableOpacity
              onPress={() => navigate("SoonPay")}
              style={[styles.buttonFooter, styles.buttonBuyColor]}
            >
              <Text style={styles.textButton}>Pagar</Text>
            </TouchableOpacity>
          </View>
        </Container>
      );
    }
  }
}

const mapStateToProps = state => {
  return {
    cartItems: state.cartItems
  };
};

const mapDispatchToProps = dispatch => {
  return {
    removeFromCart: id => dispatch({ type: "REMOVE_FROM_CART", payload: id }),
    resetAllCart: () => dispatch({ type: "RESET_CART" })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewSaleProductDetails);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#f7f7f7",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "column"
  },
  header: {
    height: 60,
    paddingHorizontal: 20,
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row"
  },
  productDetails: {
    flex: 1,
    backgroundColor: "#f7f7f7",
    width: "100%",
    padding: 20,
    paddingTop: 22
  },
  addCostumer: {
    flex: 1,
    width: "100%",
    backgroundColor: "#f7f7f7",
    padding: 20,
    alignItems: "center",
    flexDirection: "row"
  },
  salesDetails: {
    flex: 1,
    backgroundColor: "#f7f7f7",
    width: "100%",
    padding: 20,
    paddingTop: 5,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  costumerDetails: {
    flex: 1,
    backgroundColor: "#f7f7f7",
    width: "100%",
    padding: 20,
    marginBottom: 15,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  detailsLeft: {
    marginRight: 15,
    alignItems: "center"
  },
  contentTotal: {
    width: "100%",
    height: 120,
    backgroundColor: "#FFF",
    justifyContent: "space-between",
    alignItems: "center",
    borderColor: "#E5E5E5",
    paddingHorizontal: 20,
    paddingVertical: 15
  },
  contentPriceTotal: {
    width: "100%",
    height: 45,
    backgroundColor: "#FFFFFF",
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 20,
    paddingVertical: 10,
    elevation: 2
  },
  priceTotal: {
    fontSize: 22,
    color: "black"
  },
  textTotal: {
    color: "black",
    fontSize: 14
  },
  itemList: {
    width: "95%",
    flexDirection: "column",
    marginVertical: 15
  },
  infoProduct: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start",
    width: "100%"
  },
  itemListText: {
    marginLeft: 8,
    justifyContent: "center",
    flex: 7
  },
  itemListTrash: {
    marginLeft: 8,
    justifyContent: "center",
    flex: 1
  },
  itemListPrice: {
    marginLeft: 8,
    justifyContent: "center",
    flex: 4
  },
  nameProduct: {
    fontWeight: "400",
    color: "#7f8c8d",
    fontSize: 13
  },
  textQuantity: {
    fontWeight: "400",
    color: "#7f8c8d",
    fontSize: 13,
    color: "#4834d4"
  },
  textPhotoProfile: {
    marginTop: 5,
    fontWeight: "400",
    fontSize: 12,
    color: "#4834d4"
  },
  imageProfile: {
    width: 70,
    height: 70,
    borderRadius: 15
  },
  imageProduct: {
    width: 45,
    height: 45,
    borderRadius: 6,
    marginRight: 5,
    borderRadius: 10,
    backgroundColor: "white"
  },
  title: {
    fontSize: 20,
    color: "black",
    fontWeight: "600",
    marginBottom: 10
  },
  separator: {
    backgroundColor: "#ecf0f1",
    height: 1,
    width: "100%",
    marginVertical: 5
  },
  sectionTotal: {
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-between"
  },
  icon: {
    fontSize: 20,
    color: "#d63031"
  },
  iconTrash: {
    fontSize: 15,
    color: "#d63031"
  },
  footer: {
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  buttonFooter: {
    elevation: 2,
    padding: 5,
    height: 55,
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center"
  },
  buttonDeleteColor: {
    flex: 1,
    backgroundColor: "#ecf0f1"
  },
  buttonBackColor: {
    flex: 3,
    backgroundColor: Colors.morado_oscuro
  },
  buttonBuyColor: {
    flex: 4,
    backgroundColor: Colors.first
  },
  textButton: {
    color: "white",
    fontSize: 16
  },
  modalContent: {
    flexDirection: "column",
    backgroundColor: "white",
    borderRadius: 8,
    padding: 30,
    height: 150,
    width: 250,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center"
  },
  modalIcon: {
    color: "gray",
    fontSize: 35,
    marginBottom: 10,
    marginTop: 10
  },
  modalText: {
    marginTop: 20,
    textAlign: "center"
  },
  finishButton: {
    backgroundColor: "#3F46AD",
    borderRadius: 6,
    padding: 12,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    margin: 15,
    width: 60
  }
});
