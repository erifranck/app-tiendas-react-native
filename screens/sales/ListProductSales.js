import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  ActivityIndicator,
  TouchableOpacity
} from "react-native";
import {
  Container,
  Header,
  Body,
  Picker,
  Title,
  Right,
  Left,
  Button
} from "native-base";
import API from "../../utils/api";
import Icon from "react-native-vector-icons/Feather";
import NumericInput from "react-native-numeric-input";

export default class ListProducts extends React.Component {
  constructor() {
    super();
    this.state = {
      inventory: [],
      loading: true,
      selected: "Key1"
    };
  }

  async componentDidMount() {
    const products = await API.getInventory();
    this.setState({
      inventory: products,
      loading: false
    });
  }

  onValueChange(value) {
    this.setState({
      selected: value
    });
  }

  fitImage(image) {
    let fitImage = image.split("/upload/");
    return (
      // "https://res.cloudinary.com/komercia-store/image/upload/c_thumb,q_auto:best,f_auto/" +
      "https://res.cloudinary.com/komercia-store/image/upload/w_200,q_auto:best,f_auto/" +
      fitImage[1]
    );
  }
  _getProducts() {
    const { navigate } = this.props.navigation;
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#f14b5a" />
        </View>
      );
    }

    return (
      <FlatList
        style={styles.flatList}
        data={this.state.inventory}
        keyExtractor={(item, _) => item.nombre}
        renderItem={({ item }) => (
          <View style={styles.itemList}>
            <View style={styles.infoProduct}>
              <TouchableOpacity
                onPress={() =>
                  navigate("DetailsProducts", {
                    product: item
                  })
                }
              >
                <Image
                  style={styles.imageProduct}
                  resizeMode="cover"
                  source={{ uri: this.fitImage(item.foto_cloudinary) }}
                />
              </TouchableOpacity>
              <View style={styles.itemListText}>
                <Text style={styles.nameProduct}>{item.nombre}</Text>
                <Text style={{ color: "#1B1464" }}>
                  <Icon name="layers" /> {item.visitas} Unidades
                </Text>
                <View style={styles.pricingItem}>
                  <Text style={styles.priceProduct}>$ {item.precio}</Text>
                  <NumericInput
                    value={this.state.value}
                    onChange={value => this.setState({ value })}
                    totalWidth={95}
                    totalHeight={35}
                    iconSize={32}
                    step={1}
                    minValue={0}
                    valueType="real"
                    rounded
                    textColor="#222f3e"
                    iconStyle={{ color: "white" }}
                    rightButtonBackgroundColor="#576574"
                    leftButtonBackgroundColor="#576574"
                  />
                </View>
              </View>
            </View>

            <View style={styles.separator} />
          </View>
        )}
      />
    );
  }
  header() {
    const { navigate } = this.props.navigation;
    return (
      <Header style={{ backgroundColor: "#2f3542" }}>
        <Left>
          <Button transparent onPress={() => navigate("Dash")}>
            <Icon name="arrow-left" color="white" size={20} />
          </Button>
        </Left>
        <Body>
          <Title>Inventario</Title>
        </Body>
        <Right>
          <TouchableOpacity
            style={styles.cancelButton}
            onPress={() => navigate("NewSaleBarcode")}
          >
            <Text style={{ color: "black", fontSize: 13 }}>Escaner </Text>
            <Image
              style={{
                width: 36,
                height: 25,
                alignSelf: "center",
                marginLeft: 4
              }}
              source={require("../../src/assets/barcode2.png")}
            />
          </TouchableOpacity>
        </Right>
      </Header>
    );
  }

  createCostumer = () => {
    const { navigate } = this.props.navigation;
    return (
      <TouchableOpacity
        style={styles.contentCreate}
        onPress={() => navigate("NewCostumer")}
      >
        <View style={styles.cicrleCreate}>
          <Icon name="plus" style={{ color: "white", fontSize: 18 }} />
        </View>
        <Text style={TextStyle.textLink}>Crear Cliente</Text>
      </TouchableOpacity>
    );
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container style={{ backgroundColor: "white" }}>
        {/* {this.header()} */}
        {this._getProducts()}
        {/* <View style={styles.footer}>
          <Button
            onPress={() => navigate('NewSaleGeneralDetails')}
            block
            style={{
              margin: 5,
              elevation: 4,
              marginHorizontal: 18,
              backgroundColor: '#0c2461'
            }}
          >
            <Text style={{ color: 'white', fontSize: 20, marginLeft: 20 }}>
              $ 350.000
            </Text>
            <Icon
              style={{
                color: 'white',
                paddingLeft: 35,
                fontSize: 18
              }}
              name="arrow-right"
            />
          </Button>
        </View> */}
        <View style={styles.footer}>
          <Button
            onPress={() => navigate("NewSaleGeneralDetails")}
            block
            style={{
              margin: 5,
              elevation: 4,
              marginHorizontal: 18,
              backgroundColor: "#4834d4"
            }}
          >
            <Text style={{ color: "white", fontSize: 20 }}>VENDER</Text>
          </Button>
        </View>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    backgroundColor: "#f7f7f7",
    alignItems: "center",
    justifyContent: "space-around"
  },
  topText: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    height: 40,
    marginHorizontal: 10,
    paddingHorizontal: 15
  },
  length: {
    fontSize: 16,
    color: "#4c4c4c"
  },
  picker: {
    width: 145,
    alignItems: "flex-end"
  },
  flatList: {
    width: "100%",
    marginTop: 10
  },
  itemList: {
    width: "95%",
    flexDirection: "column",
    marginHorizontal: 10
  },
  infoProduct: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start",
    paddingRight: 10
  },
  itemListText: {
    flex: 3,
    width: "auto",
    marginLeft: 20,
    justifyContent: "flex-start"
  },
  itemListTextLeft: {
    marginLeft: 20,
    justifyContent: "flex-end",
    alignItems: "flex-end"
  },
  nameProduct: {
    fontWeight: "400",
    color: "#7f8c8d",
    fontSize: 15
  },
  imageProduct: {
    flex: 1,
    height: 90,
    width: 90,
    borderRadius: 6
  },
  priceProduct: {
    fontWeight: "100",
    color: "#2c3e50",
    fontSize: 20
  },
  pricingItem: {
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "space-between"
  },
  separator: {
    backgroundColor: "#ecf0f1",
    height: 1,
    width: "100%",
    marginVertical: 10
  },
  statsProduct: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 15,
    marginBottom: 5
  },
  buttonProduct: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  buttonDetails: {
    fontWeight: "bold",
    color: "#008c9d"
  },
  buttonEdit: {
    fontWeight: "bold",
    color: "#0f9380"
  },
  icon: {
    color: "white",
    fontSize: 18,
    marginLeft: 6
  },
  iconView: {
    color: "#34495e",
    fontSize: 16
  },
  cancelButton: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#FCDA92",
    borderRadius: 5,
    paddingHorizontal: 7,
    borderColor: "red",
    paddingVertical: 4,
    elevation: 3
  },
  contentTotal: {
    flexDirection: "row",
    height: 30,
    paddingHorizontal: 20,
    height: 40,
    backgroundColor: "#303456",
    justifyContent: "space-around",
    alignItems: "center",
    borderColor: "#E5E5E5",
    borderTopWidth: 0.5,
    paddingHorizontal: 5,
    elevation: 3
  },
  priceTotal: {
    fontSize: 22,
    color: "white"
  },

  textTotal: {
    color: "white"
  },
  contentCreate: {
    flexDirection: "row",
    width: "90%",
    height: 50,
    marginTop: 10,
    alignItems: "center",
    justifyContent: "flex-start"
  },
  cicrleCreate: {
    backgroundColor: "#0F9380",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
    height: 38,
    width: 38,
    marginRight: 10,
    elevation: 2
  }
});
