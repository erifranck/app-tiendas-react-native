import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";

export default class SoonPay extends React.Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <View>
          <Text style={styles.title}>
            Este modulo se encuentra en construcción, muy pronto estará listo.
            Aquí podras generar links de pago para tus clientes en redes
            sociales, ademas contabilizar las compras que tengas en tu punto de
            venta.
          </Text>
        </View>
        <Image
          style={{
            width: 220,
            height: 220,
            alignSelf: "center",
            marginBottom: 0,
            marginTop: 10
          }}
          source={require("../../src/assets/soon.jpg")}
        />

        <Text style={styles.subtitle}>
          Cualquier inquietud no dudes en contactar nuestro equipo de soporte.
        </Text>
        <TouchableOpacity
          onPress={() => navigate("Dash")}
          style={styles.capture}
        >
          <Text style={{ color: "white" }}>Volver al inicio</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    backgroundColor: "#F6F6F6",
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "space-around"
  },
  header: {
    flex: 8,
    alignContent: "center",
    justifyContent: "center"
  },
  footer: {
    flex: 1
  },
  socialIcon: {
    padding: 30
  },
  imageBack: {
    flex: 1,
    width: "100%",
    height: "100%",
    padding: 20,
    justifyContent: "center",
    alignItems: "center"
  },
  capture: {
    flex: 0,
    backgroundColor: "#3F46AD",
    borderRadius: 2,
    padding: 12,
    paddingHorizontal: 90,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    margin: 15
  },
  title: {
    fontSize: 17,
    color: "black",
    padding: 25,
    textAlign: "center"
  },
  subtitle: {
    fontSize: 14,
    color: "black",
    paddingHorizontal: 25,
    textAlign: "center"
  }
});
