// import React from "react";
// import {
//   StyleSheet,
//   View,
//   TouchableOpacity,
//   ActivityIndicator,
//   Image
// } from "react-native";
// import { Text } from "native-base";
// import { RNCamera } from "react-native-camera";

// export default class NewSaleProductDetails extends React.Component {
//   constructor() {
//     super();
//   }
//   onBarCodeRead = e => {
//     this.props.navigation.navigate("NewProductPhoto", { barcodes: e });
//   };

//   render() {
//     const { navigate } = this.props.navigation;
//     return (
//       <View style={styles.container}>
//         <View style={styles.description}>
//           <Image
//             style={styles.photo}
//             source={require("../../src/assets/camisa-example.jpg")}
//           />
//           <View style={styles.descriptionDetails}>
//             <Text>Camiste llanera roja manga larga</Text>
//           </View>
//         </View>

//         <View style={styles.salesDetails}>
//           <Text>Sales Details</Text>
//         </View>

//         <View style={styles.quantity}>
//           <Text>Sales Details</Text>
//         </View>

//         <View style={styles.footer}>
//           <Text style={styles.textFooter}>
//             Si el producto no tiene codigo {"\n"}
//             de barras, presiona "siguiente"
//           </Text>
//           <TouchableOpacity
//             onPress={() => navigate("NewProductPhoto")}
//             style={styles.capture}
//           >
//             <Text style={{ color: "white" }}>Siguiente</Text>
//           </TouchableOpacity>
//         </View>
//       </View>
//     );
//   }
// }
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: "white",
//     alignItems: "center",
//     justifyContent: "space-between",
//     flexDirection: "column"
//   },
//   description: {
//     flex: 2,
//     flexDirection: "row",
//     width: "90%"
//   },
//   photo: {
//     width: 140,
//     height: 140,
//     alignSelf: "center",
//     marginBottom: 50
//   },
//   descriptionDetails: {
//     width: "100%",
//     margin: 20
//   },
//   salesDetails: {
//     flex: 1,
//     backgroundColor: "#F1F1F2",
//     width: "100%",
//     padding: 20
//   },
//   quantity: {
//     flex: 1,
//     backgroundColor: "#FFFFFF",
//     width: "100%",
//     padding: 20
//   },
//   footer: {
//     flex: 1,
//     width: "100%",
//     flexDirection: "row",
//     backgroundColor: "#fff",
//     alignItems: "center",
//     alignContent: "center",
//     justifyContent: "center"
//   },
//   textFooter: {
//     fontSize: 12
//   },
//   capture: {
//     flex: 0,
//     backgroundColor: "#3F46AD",
//     borderRadius: 2,
//     padding: 12,
//     paddingHorizontal: 20,
//     alignSelf: "center",
//     justifyContent: "center",
//     alignItems: "center",
//     margin: 15
//   }
// });
