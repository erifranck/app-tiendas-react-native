import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  FlatList,
  Image
} from "react-native";
import Icon from "react-native-vector-icons/Feather";
import NavBar from "../src/components/navBar";
import TabBar from "../src/components/tabBar";
import API from "../utils/api";
import LottieView from "lottie-react-native";
import FastImage from "react-native-fast-image";

export default class DashBoard extends React.Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      userData: "",
      storeData: "",
      inventory: "",
      sales: "",
      feed: [
        {
          text: "Ventas",
          image:
            "https://i.pinimg.com/originals/48/a7/04/48a704e726dbaabbcbe998827cd613db.jpg"
        },
        {
          text: "Productos",
          image:
            "https://webgradients.com/public/webgradients_png/084%20Phoenix%20Start.png"
        },
        {
          text: "Visitas",
          image: "https://i.stack.imgur.com/dmWrH.png"
        }
      ]
    };
  }

  async componentDidMount() {
    const userData = await API.getUser();
    const storeData = await API.getStore();
    const products = await API.getInventory();
    const sales = await API.getSales();

    this.setState({
      userData: userData,
      storeData: storeData,
      inventory: products.slice(0, 5),
      sales: sales.ventas,
      loading: false
    });
  }

  formatCurrency = n => {
    if (n) {
      return parseInt(n)
        .toFixed()
        .replace(/(\d)(?=(\d{3})+(,|$))/g, "$1,");
    } else {
      return 0;
    }
  };

  salesValue = () => {
    let suma = (items, prop) => {
      return items.reduce((a, b) => {
        return a + b[prop];
      }, 0);
    };

    let total = suma(this.state.sales, "total");
    return total;
  };

  productViews(item) {
    if (item.length) {
      return (
        <Text style={{ paddingHorizontal: 10 }}>
          {" "}
          {item[0].numero_visitas}{" "}
        </Text>
      );
    } else {
      return <Text style={{ paddingHorizontal: 10 }}> 0 </Text>;
    }
  }

  getHorizontalFeed() {
    const { navigate } = this.props.navigation;
    return (
      <ImageBackground
        source={require("../src/assets/background.png")}
        style={styles.layoutFeed}
      >
        {/* <Text style={styles.titleLayout}>Novedades</Text> */}
        <FlatList
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          data={this.state.feed}
          keyExtractor={(item, _) => item.image}
          renderItem={({ item }) => (
            <TouchableOpacity
              onPress={() =>
                navigate("DetailsProducts", {
                  product: item
                })
              }
            >
              <ImageBackground
                style={styles.feedContent}
                source={{
                  uri: item.image
                }}
              >
                <Text style={styles.textFeedContent}>{item.text}</Text>
              </ImageBackground>
            </TouchableOpacity>
          )}
        />
      </ImageBackground>
    );
  }

  getHorizontalInventory() {
    const { navigate } = this.props.navigation;
    return (
      <ImageBackground
        source={require("../src/assets/background.png")}
        style={styles.layoutFeed}
      >
        <Text style={styles.titleLayout}>Nuevos productos</Text>
        <FlatList
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          data={this.state.inventory}
          keyExtractor={(item, _) => item.nombre}
          renderItem={({ item }) => (
            <TouchableOpacity
              onPress={() =>
                navigate("DetailsProducts", {
                  product: item
                })
              }
            >
              <ImageBackground
                onPress={() =>
                  navigate("DetailsProducts", {
                    product: item
                  })
                }
                style={styles.InventoryContent}
                source={{
                  uri: this.fitImage(item.foto_cloudinary)
                }}
              />
              <Text
                style={{
                  color: "gray",
                  alignSelf: "center",
                  fontWeight: "bold",
                  color: "black"
                }}
              >
                <Icon name="eye" size={14} color="black" />
                {this.productViews(item.visitas_producto)}

                <Text style={{ fontWeight: "300", fontSize: 11 }}>vistas</Text>
              </Text>
            </TouchableOpacity>
          )}
        />
      </ImageBackground>
    );
  }

  fitImage(image) {
    let fitImage = image.split("/upload/");
    return (
      "https://res.cloudinary.com/komercia-store/image/upload/w_550,q_auto:best/" +
      fitImage[1]
    );
  }

  render() {
    const { navigate } = this.props.navigation;
    if (this.state.loading) {
      return (
        <View style={styles.containerLoading}>
          {/* <ActivityIndicator size="large" color="#f14b5a" />
          <Text style={{ textAlign: "center" }}>Cargando Información</Text> */}
          <LottieView
            style={{
              justifyContent: "center",
              backgroundColor: "white"
            }}
            autoPlay
            loop
            source={require("../src/animations/bag.json")}
          />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <ScrollView style={styles.body}>
          <NavBar
            user={this.state.userData}
            store={this.state.storeData}
            navigation={this.props.navigation}
          />
          <View style={styles.menu_container}>
            {/* <View style={styles.layoutFeed}>{this.getHorizontalFeed()}</View>
            <View style={styles.layoutFeed}>
              {this.getHorizontalInventory()}
            </View> */}
            {/* <Image
              source={require("../src/assets/info_dash.png")}
              style={{
                width: "100%",
                height: 280
              }}
            /> */}
            {/* <View style={styles.toolsTextContent}>
              <Text style={styles.titleLayout}>Herramientas</Text>
            </View> */}
            <View style={styles.imageBack}>
              <ImageBackground
                style={{
                  width: "100%",
                  height: 115,
                  borderRadius: 5
                }}
                imageStyle={{ borderRadius: 8 }}
                source={require("../src/assets/clipped.png")}
              >
                <View
                  style={{
                    padding: 21,
                    height: "100%",
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                    alignContent: "center"
                  }}
                >
                  <View style={styles.textBackCard}>
                    <Text style={styles.textBackCard}>
                      {/* <Icon name="aperture" style={styles.tabIcon} /> */}
                      Productos{" "}
                    </Text>
                    <Text style={styles.secondTextBackCard}>
                      {this.state.inventory.length}
                    </Text>
                  </View>
                  <View style={styles.textBackCard}>
                    <Text style={styles.textBackCard}>
                      {" "}
                      {/* <Icon name="award" style={styles.tabIcon} />  */}
                      Ventas{" "}
                    </Text>
                    <Text style={styles.secondTextBackCard}>
                      {this.state.sales.length}
                    </Text>
                  </View>
                  <View style={styles.textBackCard}>
                    <Text style={styles.textBackCard}>
                      {" "}
                      {/* <Icon name="heart" style={styles.tabIcon} />  */}
                      Ingresos{" "}
                    </Text>
                    <Text style={styles.secondTextBackCard}>
                      $ {this.formatCurrency(this.salesValue())}
                    </Text>
                  </View>
                </View>
              </ImageBackground>
            </View>

            <View style={styles.menu_buttons2}>
              <TouchableOpacity
                style={styles.menuItem2}
                onPress={() => navigate("NewProductInfo")}
              >
                <Image source={require("../src/assets/iconbluebox.png")} />
                <Text style={styles.menuTitle}>Crear Producto</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.menuItem2}
                onPress={() => navigate("Sales")}
              >
                <Image source={require("../src/assets/iconpurple.png")} />
                <Text style={styles.menuTitle}>Ventas</Text>
              </TouchableOpacity>
            </View>

            <View style={styles.menu_buttons2}>
              <TouchableOpacity
                style={styles.menuItem2}
                onPress={() => navigate("Settings")}
              >
                <Image source={require("../src/assets/icongreen.png")} />
                <Text style={styles.menuTitle}>Mi perfil</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.menuItem2}
                onPress={() => navigate("Helpdesk")}
              >
                <Image source={require("../src/assets/iconorange.png")} />
                <Text style={styles.menuTitle}>Ayuda</Text>
              </TouchableOpacity>
            </View>

            <View style={styles.menu_buttons2}>
              <TouchableOpacity
                style={styles.menuItem2}
                onPress={() => navigate("ListProducts")}
              >
                <Image source={require("../src/assets/iconbag.png")} />
                <Text style={styles.menuTitle}>Inventario</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.menuItem2}
                onPress={() => navigate("ListCostumers")}
              >
                <Image source={require("../src/assets/iconhearth.png")} />
                <Text style={styles.menuTitle}>Clientes</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
        <TabBar navigation={this.props.navigation} />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    backgroundColor: "#F6F7FA",
    justifyContent: "center",
    alignContent: "center"
  },
  containerLoading: {
    flex: 1,
    width: "100%",
    backgroundColor: "red",
    justifyContent: "center",
    alignContent: "center",
    alignContent: "center"
  },
  body: {
    flex: 1,
    backgroundColor: "#F6F7FA"
  },
  menu_container: {
    marginBottom: 20
  },
  menu_buttons: {
    marginTop: 6,
    marginBottom: 8,
    flexDirection: "row",
    justifyContent: "space-around",
    paddingHorizontal: 10
  },
  menuItem: {
    width: "45%",
    height: 80,
    borderRadius: 5,
    elevation: 2,
    alignItems: "center",
    justifyContent: "center"
  },
  menu_button_bottom: {
    paddingTop: 6,
    paddingBottom: 6,
    backgroundColor: "#DCDEE5",
    flexDirection: "row",
    justifyContent: "space-around",
    paddingHorizontal: 10,
    elevation: 1,
    borderColor: "#E5E5E5",
    borderTopWidth: 0.5
  },
  menuNewSale: {
    backgroundColor: "#3F46AD",
    width: "95%",
    height: 45,
    borderRadius: 5,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    elevation: 1
  },
  menuIcon: {
    color: "#313456",
    fontSize: 30
  },
  menuTitle: {
    color: "#313456",
    marginTop: 5
  },
  menuIconSale: {
    color: "white",
    fontSize: 18,
    marginRight: 10
  },
  menuTitleSale: {
    color: "white",
    fontSize: 16
  },
  flatList: {
    flex: 1
  },
  feedContent: {
    width: 200,
    height: 80,
    borderRadius: 5,
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    marginRight: 10
  },
  textFeedContent: {
    color: "white",
    fontSize: 20,
    fontWeight: "bold",
    textShadowColor: "rgba(0,0,0, .75)",
    textShadowOffset: {
      width: 2,
      height: 2
    },
    textShadowRadius: 0
  },
  InventoryContent: {
    width: 110,
    height: 100,
    borderRadius: 5,
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    marginRight: 10,
    backgroundColor: "white"
  },
  textFeedContent: {
    color: "white",
    fontSize: 18,
    fontWeight: "bold",
    textShadowColor: "rgba(0,0,0, .75)",
    textShadowOffset: {
      width: 2,
      height: 2
    },
    textShadowRadius: 0
  },
  layoutFeed: {
    paddingVertical: 8,
    paddingHorizontal: 8
  },
  titleLayout: {
    color: "#4c4c4c",
    fontSize: 14,
    marginBottom: 10,
    fontWeight: "600"
  },
  toolsTextContent: {
    paddingHorizontal: 17,
    marginTop: 12,
    width: "100%"
  },
  imageBack: {
    width: "100%",
    height: 160,
    borderRadius: 10,
    marginTop: 10,
    marginBottom: 0,
    flexDirection: "row",
    justifyContent: "space-around",
    paddingHorizontal: 18
  },
  detailsBackCard: {
    padding: 10
  },
  textBackCard: {
    color: "white",
    fontSize: 14
  },
  secondTextBackCard: {
    color: "white",
    fontSize: 18,
    fontWeight: "600",
    marginLeft: 4
  },
  button1: {
    color: "#00E4E5"
  },
  tabIcon: {
    color: "#fff",
    fontSize: 12,
    marginBottom: 8,
    fontWeight: "100"
  },
  button2: {
    color: "#E1CE66"
  },
  menuItem2: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start",
    flex: 1
  },
  menu_buttons2: {
    marginTop: 8,
    marginBottom: 10,
    flexDirection: "row",
    justifyContent: "flex-start",
    paddingLeft: 20
  }
});
