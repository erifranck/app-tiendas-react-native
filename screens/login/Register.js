import React from "react";
import API from "../../utils/api";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  Linking
} from "react-native";
import { connect } from "react-redux";
import { Container, Item, Button } from "native-base";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { Colors } from "../../utils/const";

class Register extends React.Component {
  constructor() {
    super();
    this.state = {
      email: " ",
      password: " ",
      loading: false,
      messageError: false
    };
  }
  onPressLogin = () => {
    this.setState({ loading: true });
    API.postLogin(this.state.email, this.state.password)
      .then(response => {
        let token = response.data.access_token;
        this.props.dispatch({
          type: "LOGIN",
          payload: {
            token
          }
        });
      })
      .catch(() => {
        this.setState({
          loading: false,
          messageError: true
        });
      });
  };
  _getButtonLogin() {
    if (this.state.loading) {
      return (
        <TouchableOpacity style={styles.buttonLogin}>
          <ActivityIndicator size="large" color="#ffff" />
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          onPress={this.onPressLogin}
          style={styles.buttonLogin}
        >
          <View style={styles.alignButton}>
            <Text style={styles.buttonText}>Iniciar Sesión</Text>
          </View>
        </TouchableOpacity>
      );
    }
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <KeyboardAwareScrollView
        style={{ backgroundColor: "transparent", flex: 1, height: "100%" }}
      >
        <Container style={styles.container}>
          {/* <View style={styles.container}> */}
          <View style={styles.header}>
            <Image
              style={{
                width: 140,
                height: 33,
                alignSelf: "center",
                marginBottom: 50
              }}
              resizeMode="contain"
              source={require("../../src/assets/komercianegro.png")}
            />
          </View>

          {/* <Form style={styles.form}> */}
          {this.state.messageError == true && (
            <View style={styles.errorLogin}>
              <Text style={styles.textError}>
                La aplicación Komercia POS: Inventario, ventas y clientes, aun
                se encuentra en pruebas con usuarios seleccionados, pronto se
                pondrá a disposición del público.
              </Text>
            </View>
          )}

          <View style={styles.errorLogin}>
            <Text style={styles.textError}>
              La aplicación Komercia POS aun se encuentra en pruebas con
              usuarios seleccionados, pronto se pondrá a disposición del
              público.
            </Text>
            <Text style={styles.textError}>
              Si te gustaría hacer parte de nuestros primeros usuarios y acceder
              a la primera versión de beta escribeme:
            </Text>
            <View>
              <Button
                success
                style={{
                  marginTop: 10,
                  marginBottom: 10,
                  paddingHorizontal: 20,
                  elevation: 2
                }}
                onPress={() => {
                  Linking.openURL(
                    "http://api.whatsapp.com/send?phone=57" + 3123760628
                  );
                }}
              >
                <Text style={{ color: "#fff", fontWeight: "500" }}>
                  Escribir a WhatsApp
                </Text>
              </Button>
              <Text style={styles.textError}>gustavo@komercia.co</Text>
            </View>
          </View>

          <View style={styles.forgetPass}>
            <Text style={styles.link} onPress={() => navigate("LoginMail")}>
              <Text style={{ fontWeight: "500", fontSize: 14 }} />
              <Text
                style={{
                  fontWeight: "700",
                  color: Colors.badge,
                  fontSize: 14
                }}
              >
                Volver al inicio
              </Text>
            </Text>
          </View>

          <View style={styles.footer}>
            <Text style={styles.textFooter}>
              Al hacer click aceptas
              <Text style={styles.link} onPress={() => navigate("Terms")}>
                {" "}
                los terminos y condiciones |{" "}
              </Text>
              <Text style={styles.link} onPress={() => navigate("Terms")}>
                políticas de privacidad
              </Text>{" "}
              de Komercia.co
            </Text>
          </View>
        </Container>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    backgroundColor: "transparent",
    alignItems: "center",
    justifyContent: "space-around",
    flexDirection: "column",
    backgroundColor: "#dcf4fb"
  },
  header: {
    flex: 2,
    alignContent: "center",
    justifyContent: "center",
    backgroundColor: "#dcf4fb",
    paddingTop: 20,
    flexDirection: "column"
  },
  textWelcome: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 5
  },
  form: {
    flex: 5,
    alignContent: "center",
    justifyContent: "center",
    alignItems: "center"
  },
  title: {
    fontSize: 18,
    fontWeight: "700",
    textAlign: "center",
    marginBottom: 5,
    color: "#59617b"
  },
  subtitle: {
    fontSize: 15,
    fontWeight: "500",
    textAlign: "center",
    marginBottom: 10,
    color: "#59617b"
  },
  linkRegister: {
    color: "#59617b",
    alignSelf: "center",
    fontSize: 14,
    fontWeight: "600"
  },
  link: {
    color: "#59617b",
    alignSelf: "center",
    fontSize: 12,
    fontWeight: "600"
  },
  footer: {
    flex: 2,
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 10,
    width: 300
  },
  input: {
    backgroundColor: "#ffffff",
    height: 50,
    elevation: 2,
    width: 300,
    paddingLeft: 45,
    borderRadius: 5,
    color: "#59617b",
    fontWeight: "600",
    marginBottom: 10,
    shadowOpacity: 0.3,
    shadowRadius: 3,
    shadowColor: "#59617b",
    shadowOffset: { width: 0, height: 2 }
  },
  textFooter: {
    textAlign: "center"
  },
  alignButton: {
    flexDirection: "row",
    alignItems: "center"
  },
  buttonLogin: {
    width: 300,
    // backgroundColor: "#f14b5a",
    backgroundColor: Colors.first,
    height: 45,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginBottom: 8
  },
  buttonRegister: {
    width: 300,
    // backgroundColor: "#f14b5a",
    backgroundColor: "#eeeeee",
    height: 45,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginBottom: 8
  },
  buttonText: {
    color: "#fff",
    textAlign: "center",
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: 16,
    fontWeight: "900"
  },
  buttonTextRegister: {
    color: "#4c4c4c",
    textAlign: "center",
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: 16,
    fontWeight: "400"
  },
  forgetPass: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 25,
    width: 300
  },
  footer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 15,
    width: 300
  },
  textFooter: {
    color: "#59617b",
    alignSelf: "center",
    textAlign: "center",
    fontSize: 12
  },
  errorLogin: {
    flex: 6,
    padding: 15,
    backgroundColor: "white",
    width: "85%",
    borderRadius: 5,
    fontWeight: "500",
    justifyContent: "center",
    alignItems: "center"
  },
  textError: {
    color: Colors.morado_oscuro,
    fontSize: 14,
    fontWeight: "500",
    marginBottom: 15,
    textAlign: "center"
  }
});

const mapStateToProps = state => {
  return { auth: state };
};

export default connect(mapStateToProps)(Register);
