import React from "react";
import API from "../../utils/api";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity
} from "react-native";

export default class ForgetPass extends React.Component {
  state = {
    email: "",
    password: ""
  };
  handleEmail = text => {
    this.setState({ email: text });
  };
  handlePassword = text => {
    this.setState({ password: text });
  };
  login = (email, pass) => {
    alert("email: " + email + " password: " + pass);
  };
  onPressRecovery = () => {};
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image
            style={{
              width: 130,
              height: 23,
              alignSelf: "center",
              marginBottom: 50
            }}
            source={require("../../src/assets/komercianegro.png")}
          />
          {/* <Image
            resizeMode={"cover"}
            style={{ width: 280, height: 150 }}
            source={{
              uri:
                "https://www.tibco.com/blog/wp-content/uploads/2017/09/rsz_bigstock-mobile-app-design-and-user-int-172296434.jpg"
            }}
          /> */}
        </View>

        <View style={styles.textWelcome}>
          <Text style={styles.subtitle}>
            Escribe el correo de tu cuenta de usuario para enviarte una nueva
            contraseña
          </Text>
        </View>

        <View style={styles.form}>
          <View>
            <TextInput
              placeholderTextColor="#59617b"
              placeholder={"Escribe tu correo"}
              style={styles.input}
              underlineColorAndroid="transparent"
            />
          </View>

          <TouchableOpacity
            onPress={this.onPressRecovery}
            style={styles.buttonLogin}
          >
            <View style={styles.alignButton}>
              <Text
                style={styles.buttonText}
                onPress={() => navigate("ForgetPass")}
              >
                Recuperar constraseña
              </Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={styles.footer}>
          <Text style={styles.link} onPress={() => navigate("LoginMail")}>
            Iniciar Sesión
          </Text>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    backgroundColor: "transparent",
    alignItems: "center",
    justifyContent: "space-around",
    flexDirection: "column",
    backgroundColor: "#dcf4fb"
  },
  header: {
    flex: 4,
    alignContent: "center",
    justifyContent: "center",
    backgroundColor: "#dcf4fb",
    paddingTop: 20,
    flexDirection: "column"
  },
  textWelcome: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 5
  },
  form: {
    flex: 3,
    alignContent: "center",
    justifyContent: "center"
  },
  title: {
    fontSize: 18,
    fontWeight: "700",
    textAlign: "center",
    marginBottom: 5,
    color: "#59617b"
  },
  subtitle: {
    fontSize: 15,
    fontWeight: "500",
    textAlign: "center",
    marginBottom: 10,
    color: "#59617b"
  },
  link: {
    color: "#59617b",
    alignSelf: "center",
    fontSize: 14,
    fontWeight: "600"
  },
  footer: {
    flex: 3,
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 10,
    width: 300
  },
  input: {
    backgroundColor: "#ffffff",
    height: 50,
    width: 300,
    paddingLeft: 45,
    borderRadius: 5,
    color: "#59617b",
    fontWeight: "600",
    marginBottom: 10,
    shadowOpacity: 0.3,
    shadowRadius: 3,
    shadowColor: "#59617b",
    shadowOffset: { width: 0, height: 2 }
  },
  textFooter: {
    textAlign: "center"
  },
  alignButton: {
    flexDirection: "row",
    alignItems: "center"
  },
  buttonLogin: {
    width: 300,
    backgroundColor: "#f14b5a",
    height: 45,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginBottom: 8
  },
  buttonText: {
    color: "#fff",
    textAlign: "center",
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: 16,
    fontWeight: "900"
  }
});
