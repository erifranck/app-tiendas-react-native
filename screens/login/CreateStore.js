import React from 'react';
import API from '../../utils/api';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native';
import { connect } from 'react-redux';
import { Container, Item } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

export default class CreateStore extends React.Component {
  constructor() {
    super();
    this.state = {
      nameStore: '',
      nameUser: '',
      email: ' ',
      phone: ' ',
      password: ' ',
      loading: false,
      messageError: false
    };
  }
  onPressRegister = () => {
    this.setState({ loading: true });
    API.postLogin(this.state.email, this.state.password)
      .then(response => {
        let token = response.data.access_token;
        this.props.dispatch({
          type: 'LOGIN',
          payload: {
            token
          }
        });
      })
      .catch(() => {
        this.setState({
          loading: false,
          messageError: true
        });
      });
  };
  _getButtonRegister() {
    if (this.state.loading) {
      return (
        <TouchableOpacity style={styles.buttonLogin}>
          <ActivityIndicator size="large" color="#ffff" />
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          onPress={this.onPressRegister}
          style={styles.buttonLogin}
        >
          <View style={styles.alignButton}>
            <Text style={styles.buttonText}>Crear Cuenta</Text>
          </View>
        </TouchableOpacity>
      );
    }
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.title}>Registrate gratis</Text>
        </View>

        {this.state.messageError == true && (
          <View style={styles.errorLogin}>
            <Text style={styles.textError}>
              Error en los datos, intenta de nuevo.
            </Text>
          </View>
        )}
        <KeyboardAwareScrollView
          innerRef={ref => {
            this.scroll = ref;
          }}
        >
          <Item>
            <TextInput
              ref={input => {
                this.input1 = input;
              }}
              onSubmitEditing={() => {
                this.input1.blur();
                this.input2.focus();
              }}
              returnKeyType="next"
              blurOnSubmit={false}
              placeholderTextColor="#59617b"
              placeholder={'Nombre de tu empresa'}
              style={styles.input}
              underlineColorAndroid="rgba(0,0,0,0)"
              onChangeText={email => this.setState({ nameStore })}
            />
          </Item>

          <Item>
            <TextInput
              ref={input => {
                this.input1 = input;
              }}
              onSubmitEditing={() => {
                this.input1.blur();
                this.input2.focus();
              }}
              returnKeyType="next"
              blurOnSubmit={false}
              placeholderTextColor="#59617b"
              placeholder={'Tu nombre y apellido'}
              style={styles.input}
              underlineColorAndroid="rgba(0,0,0,0)"
              onChangeText={email => this.setState({ nameUser })}
            />
          </Item>

          <Item>
            <TextInput
              ref={input => {
                this.input1 = input;
              }}
              onSubmitEditing={() => {
                this.input1.blur();
                this.input2.focus();
              }}
              returnKeyType="next"
              blurOnSubmit={false}
              placeholderTextColor="#59617b"
              placeholder={'Telefono movil'}
              style={styles.input}
              underlineColorAndroid="rgba(0,0,0,0)"
              onChangeText={email => this.setState({ phone })}
            />
          </Item>

          <Item>
            <TextInput
              ref={input => {
                this.input1 = input;
              }}
              onSubmitEditing={() => {
                this.input1.blur();
                this.input2.focus();
              }}
              returnKeyType="next"
              blurOnSubmit={false}
              placeholderTextColor="#59617b"
              placeholder={'Correo Electrónico'}
              style={styles.input}
              underlineColorAndroid="rgba(0,0,0,0)"
              onChangeText={email => this.setState({ email })}
            />
          </Item>

          <Item>
            <TextInput
              ref={input => {
                this.input2 = input;
              }}
              blurOnSubmit={false}
              placeholderTextColor="#59617b"
              placeholder={'Tu Contraseña'}
              style={styles.input}
              secureTextEntry={true}
              underlineColorAndroid="transparent"
              onChangeText={password => this.setState({ password })}
            />
          </Item>
          {this._getButtonRegister()}
        </KeyboardAwareScrollView>

        <View style={styles.forgetPass}>
          <Text style={styles.link} onPress={() => navigate('LoginMail')}>
            Ya estoy registrado
          </Text>
        </View>

        <View style={styles.footer}>
          <Text style={styles.textFooter}>
            Al hacer click aceptas
            <Text style={styles.link} onPress={() => navigate('Terms')}>
              {' '}
              los terminos y condiciones |{' '}
            </Text>
            <Text style={styles.link} onPress={() => navigate('Terms')}>
              políticas de privacidad
            </Text>{' '}
            de Komercia.co
          </Text>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'space-around',
    flexDirection: 'column',
    backgroundColor: '#dcf4fb'
  },
  header: {
    flex: 2,
    alignContent: 'center',
    justifyContent: 'center',
    backgroundColor: '#dcf4fb',
    marginVertical: 20,
    flexDirection: 'column'
  },
  textWelcome: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 5
  },
  form: {
    flex: 5,
    alignContent: 'center',
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    fontSize: 18,
    fontWeight: '700',
    textAlign: 'center',
    marginBottom: 5,
    color: '#59617b'
  },
  subtitle: {
    fontSize: 15,
    fontWeight: '500',
    textAlign: 'center',
    marginBottom: 10,
    color: '#59617b'
  },
  link: {
    color: '#59617b',
    alignSelf: 'center',
    fontSize: 14,
    fontWeight: '600'
  },
  footer: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 10,
    width: 300
  },
  input: {
    backgroundColor: '#ffffff',
    height: 50,
    width: 300,
    paddingLeft: 45,
    borderRadius: 5,
    color: '#59617b',
    fontWeight: '600',
    marginBottom: 10,
    shadowOpacity: 0.3,
    shadowRadius: 3,
    shadowColor: '#59617b',
    shadowOffset: { width: 0, height: 2 }
  },
  textFooter: {
    textAlign: 'center'
  },
  alignButton: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  buttonLogin: {
    width: 300,
    backgroundColor: '#f14b5a',
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    marginBottom: 8
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: 16,
    fontWeight: '900'
  },
  forgetPass: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 25,
    width: 300
  },
  footer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 15,
    width: 300
  },
  textFooter: {
    color: '#59617b',
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: 12
  },
  errorLogin: {
    backgroundColor: 'white',
    marginBottom: 10,
    width: 300,
    height: 40,
    borderRadius: 5,
    fontWeight: '500',
    justifyContent: 'center',
    alignItems: 'center'
  },
  textError: {
    color: '#B1180F',
    fontSize: 14,
    fontWeight: '500'
  }
});
