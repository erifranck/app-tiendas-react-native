import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/Feather";

export default class Login extends React.Component {
  state = {
    
    email: "",
    password: ""
  };

  onPressLoginFacebook = () => {
    console.warn("login con facebook");
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image
            style={{
              width: 140,
              height: 28,
              alignSelf: "center",
              marginBottom: 50
            }}
            source={require("../../src/assets/komercianegro.png")}
          />
          <Image
            resizeMode={"cover"}
            style={{ width: 280, height: 150 }}
            source={{
              uri:
                "https://www.tibco.com/blog/wp-content/uploads/2017/09/rsz_bigstock-mobile-app-design-and-user-int-172296434.jpg"
            }}
          />
        </View>

        <View style={styles.textWelcome}>
          <Text style={styles.subtitle}>
            Todo tu negocio en la palma de tu mano {"\n"}
          </Text>
        </View>

        <View style={styles.form}>
          <TouchableOpacity
            onPress={this.onPressLoginFacebook}
            style={[styles.buttonLogin, styles.facebookColor]}
          >
            <Icon name="facebook" style={styles.icon}>
              <Text style={styles.buttonText}>Ingresar con Facebook</Text>
            </Icon>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => navigate("LoginMail")}
            style={styles.buttonLogin}
          >
            <View style={styles.alignButton}>
              <Icon name="facebook" style={styles.icon} />
              <Text style={styles.buttonText}>Ingresar con email</Text>
            </View>
          </TouchableOpacity>
        </View>

        {/* <View style={styles.footer}>
            <Text style={styles.link} onPress={() => navigate("ForgetPass")}>
              ¿Olvidaste tu contraseña?
            </Text>
          </View> */}

        <View style={styles.footer}>
          <Text style={styles.textFooter}>
            Al hacer click aceptas
            <Text style={styles.link} onPress={() => navigate("Terms")}>
              {" "}
              los terminos y condiciones | políticas de privacidad
            </Text>{" "}
            de Komercia.co
          </Text>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    backgroundColor: "transparent",
    alignItems: "center",
    justifyContent: "space-around",
    flexDirection: "column",
    backgroundColor: "#dcf4fb"
  },
  header: {
    flex: 7,
    alignContent: "center",
    justifyContent: "center",
    backgroundColor: "#dcf4fb",
    paddingTop: 20,
    flexDirection: "column"
  },
  textWelcome: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 5
  },
  form: {
    flex: 3,
    alignContent: "center",
    justifyContent: "center",
    marginBottom: 20
  },
  title: {
    fontSize: 18,
    fontWeight: "700",
    textAlign: "center",
    marginBottom: 5,
    color: "#59617b"
  },
  subtitle: {
    fontSize: 15,
    fontWeight: "500",
    textAlign: "center",
    marginBottom: 10,
    color: "#59617b"
  },
  textFooter: {
    textAlign: "center"
  },
  alignButton: {
    flexDirection: "row",
    alignItems: "center"
  },
  buttonLogin: {
    width: 300,
    backgroundColor: "#f14b5a",
    height: 45,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginBottom: 8
  },
  buttonText: {
    color: "#fff",
    textAlign: "center",
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: 16,
    fontWeight: "900"
  },
  icon: {
    color: "#fff",
    fontSize: 22
  },
  facebookColor: {
    backgroundColor: "#3b5998"
  },
  footer: {
    flex: 2,
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 20,
    width: 300
  },
  textFooter: {
    color: "#59617b",
    alignSelf: "center",
    textAlign: "center",
    fontSize: 12
  },
  link: { fontWeight: "800" }
});
