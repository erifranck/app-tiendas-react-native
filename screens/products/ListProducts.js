import React from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  ActivityIndicator,
  TouchableOpacity,
  Image
} from "react-native";
import {
  Container,
  Header,
  Body,
  Title,
  Right,
  Left,
  Button
} from "native-base";
import API from "../../utils/api";
import Icon from "react-native-vector-icons/Feather";
import FastImage from "react-native-fast-image";
import { formatCurrency, Colors } from "../../utils/const";
import TabBar from "../../src/components/tabBar";
import { TextStyle } from "../../utils/textStyles";

export default class ListProducts extends React.Component {
  constructor() {
    super();
    this.state = {
      inventory: [],
      loading: true,
      selected: "Key1"
    };
  }
  async componentDidMount() {
    const products = await API.getInventory();
    this.setState({
      inventory: products,
      loading: false
    });
  }
  onValueChange(value) {
    this.setState({
      selected: value
    });
  }
  fitImage(image) {
    let fitImage = image.split("/upload/");
    return (
      "https://res.cloudinary.com/komercia-store/image/upload/w_250,q_auto:best,f_auto/" +
      fitImage[1]
    );
  }
  _getProducts() {
    const { navigate } = this.props.navigation;
    if (this.state.loading) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#f14b5a" />
        </View>
      );
    }
    if (this.state.inventory.length == 0) {
      return (
        <View
          style={{
            alignItems: "center",
            alignContent: "center",
            justifyContent: "center",
            height: 450
          }}
        >
          <Image
            style={{ width: 110, height: 110, marginBottom: 20 }}
            source={require("../../src/assets/empty-box.png")}
          />
          <Text>No tienes productos registrados.</Text>
          <TouchableOpacity
            onPress={() => navigate("NewProductInfo")}
            style={{
              marginTop: 20,
              padding: 10,
              backgroundColor: Colors.first
            }}
          >
            <Text style={{ color: "white" }}>Agrega tu primer producto</Text>
          </TouchableOpacity>
        </View>
      );
    }
    return (
      <FlatList
        style={styles.flatList}
        windowSize={15}
        removeClippedSubviews={true}
        data={this.state.inventory}
        keyExtractor={(item, _) => item.nombre}
        renderItem={({ item }) => (
          <TouchableOpacity
            onPress={() =>
              navigate("DetailsProducts", {
                product: item
              })
            }
          >
            <View style={styles.itemList}>
              <View style={styles.infoProduct}>
                <FastImage
                  style={styles.imageProduct}
                  resizeMode={FastImage.resizeMode.cover}
                  source={{ uri: this.fitImage(item.foto_cloudinary) }}
                />

                <View style={styles.itemListText}>
                  <Text style={styles.nameProduct}>{item.nombre}</Text>
                  <Text style={styles.priceProduct}>
                    $ {formatCurrency(item.precio)}
                  </Text>
                  <Text>{item.inventario} unidades </Text>
                  <Text style={{ color: "#27ae60" }}>
                    <Icon name="eye" /> {item.visitas} Visitas
                  </Text>
                </View>
                {/* <View style={styles.itemListTextLeft}>
                  <Text style={{ color: "#6E6BFD" }}>
                    <Icon name="pocket" /> {item.visitas} ventas
                  </Text>
                </View> */}
              </View>
              <View style={styles.separator} />
            </View>
          </TouchableOpacity>
        )}
      />
    );
  }
  header() {
    const { navigate } = this.props.navigation;
    const { goBack } = this.props.navigation;
    return (
      <Header>
        <Left>
          <Button transparent onPress={() => goBack()}>
            <Icon name="arrow-left" color="white" size={20} />
          </Button>
        </Left>
        <Body>
          <Title>Inventario</Title>
        </Body>
        <Right>
          <TouchableOpacity
            style={styles.cancelButton}
            onPress={() => navigate("NewProductInfo")}
          >
            <Text style={{ color: "white", fontSize: 13 }}>Nuevo producto</Text>
            <Icon name="plus-circle" style={styles.icon} />
          </TouchableOpacity>
        </Right>
      </Header>
    );
  }

  createCostumer = () => {
    const { navigate } = this.props.navigation;
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          width: "95%"
        }}
      >
        <TouchableOpacity
          style={styles.contentCreate}
          onPress={() => navigate("NewProductInfo")}
        >
          <View style={styles.cicrleCreate}>
            <Icon name="plus" style={{ color: "white", fontSize: 18 }} />
          </View>
          <Text style={TextStyle.textLink}>Nuevo Producto</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.cancelButton}
          onPress={() => navigate("NewSaleBarcode")}
        >
          <Text style={{ color: "black", fontSize: 13 }}>Escaner </Text>
          <Image
            style={{
              width: 36,
              height: 25,
              alignSelf: "center"
            }}
            source={require("../../src/assets/barcode2.png")}
          />
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container style={{ backgroundColor: "white" }}>
        <View
          style={{
            alignItems: "center",
            justifyContent: "center",
            width: "100%"
          }}
        >
          {this.createCostumer()}
        </View>
        {/* {this.header()} */}
        {/* <View style={styles.topText}>
          <Text style={styles.length}>
            {this.state.inventory.length} productos
          </Text>
          <View>
            <Picker
              mode="dropdown"
              iosHeader="Select item"
              style={styles.picker}
              selectedValue={this.state.selected}
              onValueChange={this.onValueChange.bind(this)}
            >
              <Picker.Item label="Categoria" value="key0" />
              <Picker.Item label="Papeleria" value="key1" />
              <Picker.Item label="Tecnologia" value="key2" />
            </Picker>
          </View>
        </View> */}

        <View style={styles.body}>{this._getProducts()}</View>
        <TabBar navigation={this.props.navigation} />
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    backgroundColor: "#f7f7f7",
    alignItems: "center",
    justifyContent: "space-around"
  },
  body: {
    flex: 1,
    backgroundColor: "#f7f7f7"
  },
  topText: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    height: 40,
    marginHorizontal: 10,
    paddingHorizontal: 15
  },
  length: {
    fontSize: 16,
    color: "#4c4c4c"
  },
  picker: {
    width: 145,
    alignItems: "flex-end"
  },
  flatList: {
    width: "100%",
    marginTop: 10
  },
  itemList: {
    width: "95%",
    flexDirection: "column",
    marginHorizontal: 10
  },
  infoProduct: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start",
    paddingRight: 10
  },
  itemListText: {
    flex: 5,
    marginLeft: 10,
    justifyContent: "flex-start"
  },
  itemListTextLeft: {
    marginLeft: 10,
    justifyContent: "flex-end"
  },
  nameProduct: {
    fontWeight: "400",
    color: "#7f8c8d",
    fontSize: 13
  },
  imageProduct: {
    flex: 2,
    width: 80,
    height: 90,
    overflow: "hidden",
    borderRadius: 6,
    // resizeMode: 'contain',
    backgroundColor: "white"
  },
  priceProduct: {
    fontWeight: "100",
    color: "#2c3e50",
    fontSize: 20
  },
  separator: {
    backgroundColor: "#ecf0f1",
    height: 1,
    width: "100%",
    marginVertical: 10
  },
  statsProduct: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 15,
    marginBottom: 5
  },
  buttonProduct: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  buttonDetails: {
    fontWeight: "bold",
    color: "#008c9d"
  },
  buttonEdit: {
    fontWeight: "bold",
    color: "#0f9380"
  },
  icon: {
    color: "white",
    fontSize: 18,
    marginLeft: 6
  },
  iconView: {
    color: "#34495e",
    fontSize: 16
  },
  cancelButton: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  contentCreate: {
    flexDirection: "row",
    width: "40%",
    height: 50,
    marginTop: 9,
    marginBottom: 9,
    alignItems: "center",
    justifyContent: "flex-start"
  },
  cicrleCreate: {
    backgroundColor: "#eb4d4b",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
    height: 38,
    width: 38,
    marginRight: 10,
    elevation: 2
  }
});
