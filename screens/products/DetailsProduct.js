import React from "react";
import {
  StyleSheet,
  ActivityIndicator,
  Text,
  Image,
  View,
  ScrollView,
  TouchableOpacity
} from "react-native";
import { Container, Button, Badge } from "native-base";
import API from "../../utils/api";
import Icon from "react-native-vector-icons/Feather";
import Swiper from "react-native-swiper";
import { connect } from "react-redux";
import { Colors, formatCurrency } from "../../utils/const";
import FastImage from "react-native-fast-image";
import Share, { ShareSheet } from "react-native-share";
import RNFetchBlob from "rn-fetch-blob";
import NumericInput from "react-native-numeric-input";
import {addToCar} from "../../redux/reducers/cartItems";

class DetailsProducts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      productDetails: "",
      loading: true,
      visible: false,
      imageShare: "",
      cantidad: 1,
      value: 1
    };
  }

  onCancel() {
    console.log("CANCEL");
    this.setState({ visible: false });
  }
  onOpen() {
    console.log("OPEN");
    this.setState({ visible: true });
  }

  base64Image = () => {
    const item = this.props.navigation.state.params.product;
    const fs = RNFetchBlob.fs;
    let imagePath = null;
    RNFetchBlob.config({
      fileCache: true
    })
      .fetch("GET", item.foto_cloudinary)
      // the image is now dowloaded to device's storage
      .then(resp => {
        // the image path you can use it directly with Image component
        imagePath = resp.path();
        return resp.readFile("base64");
      })
      .then(base64Data => {
        // here's base64 encoded image
        this.setState({ imageShare: `data:image/png;base64,${base64Data}` });
        return `data:image/png;base64,${base64Data}`;
        // remove the file from storage
        return fs.unlink(imagePath);
      });
  };

  async componentDidMount() {
    let detailsProductsAPI = await API.getProductDetails(
      this.props.navigation.state.params.product.id
    );
    this.setState({
      productDetails: detailsProductsAPI,
      loading: false
    });
    this.base64Image();
  }

  fitImage(image) {
    let fitImage = image.split("/upload/");
    return (
      "https://res.cloudinary.com/komercia-store/image/upload/w_600,q_auto:best,f_auto/" +
      fitImage[1]
    );
  }

  showImages1 = () => {
    if (this.state.loading) {
      return (
        <View style={styles.containerLoading}>
          <ActivityIndicator size="large" color="#f14b5a" />
          <Text style={{ textAlign: "center" }}>Cargando Información</Text>
        </View>
      );
    } else {
      if (this.state.productDetails.fotos[0]) {
        return (
          <View style={styles.slide1}>
            {this.state.productDetails.fotos[0] && (
              <FastImage
                style={styles.imageProduct}
                resizeMode="contain"
                source={{
                  uri: this.fitImage(
                    this.state.productDetails.fotos[0].foto_cloudinary
                  )
                }}
              />
            )}
          </View>
        );
      }
    }
  };

  showImages2 = () => {
    if (this.state.loading) {
      return (
        <View style={styles.containerLoading}>
          <ActivityIndicator size="large" color="#f14b5a" />
          <Text style={{ textAlign: "center" }}>Cargando Información</Text>
        </View>
      );
    } else {
      if (this.state.productDetails.fotos[1]) {
        return (
          <View style={styles.slide1}>
            {this.state.productDetails.fotos[1] && (
              <Image
                style={styles.imageProduct}
                resizeMode="contain"
                source={{
                  uri: this.fitImage(
                    this.state.productDetails.fotos[1].foto_cloudinary
                  )
                }}
              />
            )}
          </View>
        );
      } else {
        <View />;
      }
    }
  };

  // handleClick = () => {
  //   const item = this.props.navigation.state.params.product;
  //   console.warn(item);
  //   this.props.addItemToCart(item);
  // };

  render() {
    const { navigate } = this.props.navigation;
    const item = this.props.navigation.state.params.product;
    const { goBack } = this.props.navigation;
    console.warn(item);

    let shareImageBase64 = {
      title: item.nombre,
      message: item.nombre,
      url: this.state.imageShare,
      type: "image/png",
      subject: "Share Link" //  for email
    };

    return (
      <Container>
        <ScrollView style={{ flex: 1, height: "100%" }}>
          <View style={styles.contentImages}>
            <View style={styles.header}>
              <Button transparent onPress={() => goBack()}>
                <Icon name="arrow-left" color="#7f8c8d" size={30} />
              </Button>
              <View style={{ flexDirection: "row" }}>
                <TouchableOpacity
                  style={styles.tabShare}
                  onPress={() => {
                    Share.open(shareImageBase64);
                  }}
                >
                  <Icon name="share-2" style={styles.tabCartIcon} />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.tabCart}
                  // onPress={this.props.addItemToCart}
                  onPress={() => navigate("NewSaleGeneralDetails")}
                >
                  <Icon name="shopping-cart" style={styles.tabCartIcon} />

                  {this.props.cartItems.length > 0 ? (
                    <Badge
                      style={{
                        width: 20,
                        height: 20,
                        position: "absolute",
                        backgroundColor: "#0000ff",
                        marginLeft: 34
                      }}
                    >
                      <Text style={{ color: "#fff" }}>
                        {this.props.cartItems.length}
                      </Text>
                    </Badge>
                  ) : (
                    <View />
                  )}
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.imageContent}>
              <Swiper
                style={styles.wrapper}
                showsButtons={false}
                dot={
                  <View
                    style={{
                      backgroundColor: "rgba(255,255,255,.3)",
                      width: 13,
                      height: 13,
                      borderRadius: 7,
                      marginLeft: 7,
                      marginRight: 7
                    }}
                  />
                }
                activeDot={
                  <View
                    style={{
                      backgroundColor: "#4c4c4c",
                      width: 13,
                      height: 13,
                      borderRadius: 7,
                      marginLeft: 7,
                      marginRight: 7
                    }}
                  />
                }
                paginationStyle={{
                  bottom: 70
                }}
                loop={false}
              >
                <View style={styles.slide1}>
                  <FastImage
                    style={styles.imageProduct}
                    resizeMode={FastImage.resizeMode.contain}
                    source={{ uri: this.fitImage(item.foto_cloudinary) }}
                  />
                </View>
              </Swiper>
            </View>
          </View>

          <View style={styles.contentDetails}>
            <Text style={styles.itemNombre}>{item.nombre.toUpperCase()}</Text>
            <Text style={styles.textStars}>
              <Icon name="star" size={15} color="#f1c40f" />{" "}
              <Icon name="star" size={15} color="#f1c40f" />{" "}
              <Icon name="star" size={15} color="#f1c40f" /> {"  "}
              Calificaciones
            </Text>
            <Text style={styles.priceProduct}>
              ${formatCurrency(item.precio)}
            </Text>

            <Text>{item.inventario} unidades disponibles</Text>

            <Text style={{ color: "#27ae60" }}>
              <Icon name="eye" /> {item.visitas} Visualizaciones
            </Text>
            <View style={styles.contentDescription} />
          </View>
        </ScrollView>
        {/* <View style={styles.footer}>
          <NumericInput
            initValue={1}
            totalWidth={120}
            totalHeight={60}
            minValue={1}
            step={1}
            inputStyle={{ fontSize: 17, width: 40 }}
            containerStyle={{ fontSize: 17 }}
            iconStyle={{ fontSize: 16 }}
            iconSize={10}
            onPress={value => this.setState({ cantidad: value })}
          />

          <TouchableOpacity
            style={styles.botonAdd}
            onPress={() => {
              this.props.addItemToCart(item);
            }}
          >
            <Text
              style={{ color: "white", alignItems: "center", fontSize: 14 }}
            >
              AÑADIR A LA VENTA
            </Text>
          </TouchableOpacity>
        </View> */}
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    cartItems: state.cartItems
  };
};
const mapDispatchToProps = dispatch => {
  return {
    addItemToCart: item => dispatch(addToCar(item))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DetailsProducts);

const styles = StyleSheet.create({
  tabCart: {
    zIndex: 9999,
    backgroundColor: Colors.red,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
    height: 42,
    width: 42,
    elevation: 6,
    marginTop: -35
  },
  tabCartIcon: {
    color: "white",
    fontSize: 18,
    fontWeight: "100"
  },
  tabShare: {
    zIndex: 9999,
    backgroundColor: Colors.first,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
    height: 42,
    width: 42,
    elevation: 6,
    marginTop: -35,
    marginRight: 6
  },
  tabShareIcon: {
    color: "white",
    fontSize: 18,
    fontWeight: "100"
  },
  botonAdd: {
    flex: 1,
    height: 60,
    backgroundColor: Colors.first,
    justifyContent: "center",
    alignItems: "center"
  },
  header: {
    backgroundColor: "white",
    flex: 1,
    paddingHorizontal: 15,
    height: 20,
    width: "98%",
    alignItems: "flex-end",
    justifyContent: "center"
  },
  contentImages: {
    flex: 3,
    width: "100%",
    height: 250
  },

  contentDetails: {
    width: "100%",
    height: "100%",
    backgroundColor: "#ecf0f1",
    padding: 15,
    flex: 2
  },
  itemNombre: {
    fontSize: 16,
    fontWeight: "100"
  },
  priceProduct: {
    fontWeight: "100",
    color: "#2c3e50",
    fontSize: 28
  },
  visitas: {
    color: "#7f8c8d"
  },
  textStars: {
    paddingVertical: 5,
    alignItems: "center"
  },
  contentDescription: {
    marginVertical: 10
  },
  imageContent: {
    backgroundColor: "blue",
    flex: 3,
    width: "100%",
    marginBottom: 15,
    marginTop: 10,
    resizeMode: "cover"
  },
  imageProductSwiper: {
    flex: 1,
    width: "100%",
    height: "100%",
    resizeMode: "cover",
    backgroundColor: "red"
  },
  imageProduct: {
    width: "100%",
    height: "100%",
    backgroundColor: "white"
  },
  slideContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  slide1: {
    flex: 1,
    height: 410,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff"
  },
  slide2: {
    backgroundColor: "rgba(20,200,20,0.3)"
  },
  slide3: {
    backgroundColor: "rgba(200,20,20,0.3)"
  },
  footer: {
    flexDirection: "row",
    elevation: 2,
    width: "100%",
    justifyContent: "space-between",
    backgroundColor: "#f7f7f7"
  }
});
