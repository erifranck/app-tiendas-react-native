import React from "react";
import { StyleSheet, View, ActivityIndicator } from "react-native";
import { Text } from "native-base";
import { RNCamera } from "react-native-camera";

export default class NewProduct extends React.Component {
  constructor() {
    super();
    this.state = { loading: false };
  }
  onBarCodeRead = e => {
    this.setState({ loading: true });

    if (e[0]) {
      this.props.navigation.state.params.returnData(e[0].data);
      this.props.navigation.goBack();
    } else {
      this.props.navigation.state.params.returnData(e.data);
      this.props.navigation.goBack();
    }
  };
  _viewCamera() {
    if (this.state.loading) {
      return (
        <ActivityIndicator
          style={styles.preview}
          size="large"
          color="#0000ff"
        />
      );
    } else {
      return (
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.on}
          onBarCodeRead={this.onBarCodeRead}
          permissionDialogTitle={"Acceso a la camara"}
          permissionDialogMessage={
            "Necesatas dar permisos para acceder a la camara"
          }
          onGoogleVisionBarcodesDetected={({ barcodes }) => {
            this.onBarCodeRead(barcodes);
          }}
        />
      );
    }
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <View style={styles.separator}>
          <Text>Pasa la camara sobre el codigo de barras</Text>
        </View>
        <View style={styles.camera}>{this._viewCamera()}</View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "column"
  },
  preview: {
    width: "100%",
    height: "95%"
  },
  camera: {
    flex: 1,
    height: "70%",
    width: "90%"
  },
  separator: {
    width: "90%",
    height: 60,
    marginBottom: 5,
    justifyContent: "center",
    alignItems: "center"
  },
  footer: {
    flex: 1,
    width: "100%",
    flexDirection: "row",
    backgroundColor: "#fff",
    alignItems: "center",
    alignContent: "center",
    justifyContent: "center"
  },
  textFooter: {
    fontSize: 12
  },
  capture: {
    flex: 0,
    backgroundColor: "#3F46AD",
    borderRadius: 2,
    padding: 12,
    paddingHorizontal: 20,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    margin: 15
  }
});
