import React from "react";
import { StyleSheet, View, TouchableOpacity, Image } from "react-native";
import { FooterTab, Button, Text } from "native-base";
import { RNCamera } from "react-native-camera";
import ImagePicker from "react-native-image-picker";
import Icon from "react-native-vector-icons/Feather";

const options = {
  title: "Select Avatar",
  customButtons: [{ name: "fb", title: "Choose Photo from Facebook" }],
  storageOptions: {
    skipBackup: true,
    path: "images"
  }
};

export default class NewProductPhoto extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tags: "No has subido ninguna imagen",
      contenido: null,
      avatarSource: ""
    };
  }
  // _galery2() {
  //   const { navigation } = this.props;
  //   const barcode = navigation.getParam("barcodes", "null");
  //   console.warn(barcode);
  // }
  _galery() {
    // ImagePicker.launchImageLibrary(options, response => {
    //   console.warn(this.props.barcodes);
    // Open Image Library:
    ImagePicker.launchImageLibrary(options, response => {
      // Same code as in above section!
    });
  }
  _camera() {
    // Launch Camera:
    ImagePicker.launchCamera(options, response => {
      // Same code as in above section!
    });
  }
  render() {
    // const { navigation } = this.props;
    const { navigate } = this.props.navigation;

    return (
      <View style={styles.container}>
        {/* <View style={styles.separator}>
          <Text>2. Fotografia del producto</Text>
        </View> */}

        <Image style={styles.preview} />

        <View style={styles.footer}>
          <TouchableOpacity
            onPress={() => navigate("NewProductInfo")}
            style={styles.capture}
          >
            <Icon
              style={{ color: "white", marginBottom: 6, fontSize: 35 }}
              name="arrow-right"
            />
            <Text style={{ color: "white", fontSize: 12 }}>Tomar Foto</Text>
          </TouchableOpacity>
          <Image source={this.state.avatarSource} style={styles.uploadAvatar} />
        </View>

        <FooterTab style={styles.footBar}>
          <Button vertical onPress={() => this._galery()}>
            <Icon name="apps" />
            <Text>Galería</Text>
          </Button>
          <Button vertical active onPress={() => this._camera()}>
            <Icon name="camera" />
            <Text>Camara</Text>
          </Button>
        </FooterTab>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  preview: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center"
  },
  capture: {
    flex: 0,
    backgroundColor: "#fff",
    borderRadius: 5,
    color: "#000",
    padding: 10,
    margin: 40
  },
  container: {
    flex: 1,
    backgroundColor: "black",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "column"
  },
  preview: {
    width: "100%",
    flex: 6,
    justifyContent: "flex-end",
    alignItems: "center",
    backgroundColor: "green"
  },
  separator: {
    width: "100%",
    flexDirection: "column",
    backgroundColor: "#ffffff",
    padding: 15,
    justifyContent: "center",
    alignItems: "center"
  },
  footer: {
    flex: 1,
    width: "100%",
    flexDirection: "row",
    backgroundColor: "#fff",
    alignItems: "center",
    alignContent: "center",
    justifyContent: "center"
  },
  textFooter: {
    fontSize: 12
  },
  footBar: {
    height: 100
  },
  capture: {
    backgroundColor: "#43A047",
    borderRadius: 5,
    padding: 10,
    elevation: 5,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    marginTop: -60
  }
});
