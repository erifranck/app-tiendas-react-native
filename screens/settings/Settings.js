import React from "react";
import { StyleSheet, Text, View, Image, ScrollView } from "react-native";
import TabBar from "../../src/components/tabBar";
import { Colors } from "../../utils/const";
import { Container, Tab, Tabs, TabHeading } from "native-base";
import Profile from "./Profile";
import Store from "./Store";
import Membership from "./Membership";

export default class Membreship extends React.Component {
  constructor() {
    super();
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <ScrollView style={styles.body}>
          {/* <View style={styles.infoHotel}>
            <Image
              style={styles.imageTop}
              source={{
                uri:
                  "https://images.pexels.com/photos/1087727/pexels-photo-1087727.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260"
              }}
            />
          </View> */}
          <Tabs tabBarUnderlineStyle={{ backgroundColor: Colors.red }}>
            <Tab
              heading={
                <TabHeading style={{ backgroundColor: "white" }}>
                  <Text style={{ color: "black" }}>Mi Perfil</Text>
                </TabHeading>
              }
            >
              <Profile navigation={this.props.navigation} />
            </Tab>
            <Tab
              heading={
                <TabHeading style={{ backgroundColor: "white" }}>
                  <Text style={{ color: "black" }}>Mi Tienda</Text>
                </TabHeading>
              }
            >
              <Store navigation={this.props.navigation} />
            </Tab>
            <Tab
              heading={
                <TabHeading style={{ backgroundColor: "white" }}>
                  <Text style={{ color: "black" }}>Suscripción</Text>
                </TabHeading>
              }
            >
              <Membership navigation={this.props.navigation} />
            </Tab>
          </Tabs>
        </ScrollView>
        <TabBar navigation={this.props.navigation} />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    justifyContent: "space-around"
  },
  imageTop: {
    width: "100%",
    height: 100
  }
});
