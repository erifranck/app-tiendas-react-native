import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Icon from "react-native-vector-icons/Feather";
import { Colors, Bold } from "../../utils/const";

export default class Benefits extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>
          Esta aplicación es completamente gratis para todos los usuarios de
          Komercia
        </Text>
        <View style={styles.itemList}>
          <View style={styles.itemInfo}>
            <Icon
              name="plus-circle"
              size={23}
              color={Colors.gold}
              style={styles.itemIcon}
            />
            <Text style={styles.itemText}>Gratis</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    padding: 20
  },
  itemList: {
    marginVertical: 20,
    width: "90%",
    alignItems: "flex-start",
    justifyContent: "center"
  },
  itemInfo: {
    flexDirection: "row",
    marginBottom: 9
  },
  itemIcon: {
    marginRight: 15,
    marginLeft: 10
  },
  itemText: {
    color: "black"
  }
});
