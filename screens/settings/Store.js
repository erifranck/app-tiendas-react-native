import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight
} from "react-native";
import Icon from "react-native-vector-icons/Feather";
import { Colors, Bold } from "../../utils/const";
import API from "../../utils/api";
import FastImage from "react-native-fast-image";
import LottieView from "lottie-react-native";

export default class Store extends React.Component {
  constructor() {
    super();
    this.state = { loading: true, storeData: "" };
  }

  async componentDidMount() {
    const storeData = await API.getStore();
    this.setState({
      storeData: storeData,
      loading: false
    });
  }

  render() {
    const { navigate } = this.props.navigation;
    if (this.state.loading) {
      return (
        <View style={styles.containerLoading}>
          <LottieView
            style={{
              justifyContent: "center",
              backgroundColor: "white"
            }}
            autoPlay
            loop
            source={require("../../src/animations/bag.json")}
          />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <FastImage
          style={styles.photoUser}
          resizeMode="contain"
          source={{
            uri: `https://api2.komercia.co/logos/${this.state.storeData.logo}`
          }}
        />
        <Text style={styles.nameUser}>{this.state.storeData.nombre}</Text>
        <View style={styles.itemList}>
          {/* <View style={styles.itemInfo}>
            <Icon
              name="sidebar"
              size={20}
              color="gray"
              style={styles.itemIcon}
            />
            <Text style={styles.itemText}>
              <Bold>NIT.</Bold> 40123654
            </Text>
          </View> */}
          {/* <View style={styles.itemInfo}>
            <Icon name="user" size={20} color="gray" style={styles.itemIcon} />
            <Text style={styles.itemText}>
              <Bold>Telefono </Bold> 57231234
            </Text>
          </View> */}
          <View style={styles.itemInfo}>
            <Icon name="disc" size={20} color="gray" style={styles.itemIcon} />
            <Text style={styles.itemText}>
              <Bold>Creado </Bold> {this.state.storeData.created_at}
            </Text>
          </View>
          {/* <View style={styles.itemInfo}>
            <Icon name="mail" size={20} color="gray" style={styles.itemIcon} />
            <Text style={styles.itemText}>
              <Bold>Correo</Bold> Byron_0613@hotmail.com
            </Text>
          </View> */}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    alignItems: "center",
    padding: 25
  },
  photoUser: {
    width: 160,
    height: 100,
    marginBottom: 10
  },
  nameUser: {
    width: 180,
    fontWeight: "600",
    fontSize: 19,
    textAlign: "center",
    color: Colors.red
  },
  itemList: {
    marginVertical: 20,
    width: "90%",
    alignItems: "flex-start",
    justifyContent: "center"
  },
  itemInfo: {
    flexDirection: "row",
    marginBottom: 7
  },
  itemIcon: {
    marginRight: 8
  },
  itemText: {
    color: "black"
  },
  webSiteLink: {
    width: "100%",
    height: 45,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
    elevation: 1,
    marginTop: 5,
    backgroundColor: "#F5F5F5"
  },
  textLink: {
    color: "gray",
    fontSize: 16,
    fontWeight: "400"
  },
  bookingButton: {
    width: "100%",
    height: 45,
    backgroundColor: Colors.red,
    justifyContent: "center",
    alignItems: "center"
  },
  bookingText: {
    color: "white",
    fontWeight: "300",
    fontSize: 16
  }
});
