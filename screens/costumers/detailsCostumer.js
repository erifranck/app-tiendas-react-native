import React from 'react';
import { StyleSheet, View, Image, ScrollView } from 'react-native';
import { Text, Button, Container, Row, Badge } from 'native-base';
import Icon from 'react-native-vector-icons/Feather';

export default class detailsCostumer extends React.Component {
  constructor() {
    super();
  }

  render() {
    const { goBack } = this.props.navigation;
    return (
      <Container>
        <ScrollView>
          <View style={styles.header}>
            <Button transparent onPress={() => goBack()}>
              <Icon name="arrow-left" color="black" size={30} />
            </Button>
          </View>

          <View style={styles.salesDetails}>
            <View>
              <Text style={styles.title}>Información </Text>

              <Text style={styles.nameProduct}>Nombre del cliente</Text>
              <Text>Ana Maria Rodriguez</Text>
              <Text style={styles.nameProduct}>Email</Text>
              <Text>Punto de venta</Text>
              <Text style={styles.nameProduct}>Identificaciin</Text>
              <Text>#11232442</Text>
              <Text style={styles.nameProduct}>Telefono</Text>
              <Text>3204323912</Text>
              <Text style={styles.nameProduct}>Dirección</Text>
              <Text>Calle 36 # 19-30</Text>
              <Text style={styles.nameProduct}>Ciudad</Text>
              <Text>Villavicencio</Text>
            </View>
            <View style={styles.detailsLeft}>
              <View>
                <Image
                  style={styles.imageProfile}
                  source={require('../../src/assets/female-pic2.jpg')}
                />
                <Text style={styles.textPhotoProfile}>30 Compras </Text>
              </View>
            </View>
          </View>

          <View style={styles.productDetails}>
            <Text style={styles.title}>Historial de Compras</Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginBottom: 8
              }}
            >
              <Text style={styles.nameProduct}>Ultima compra hace: </Text>
              <Badge success>
                <Text style={{ color: 'white', padding: 1 }}>45 días</Text>
              </Badge>
            </View>
            <View style={styles.separator} />
            <View style={styles.itemList}>
              <View style={styles.infoProduct}>
                <Image
                  style={styles.imageProduct}
                  source={require('../../src/assets/camisa-example.jpg')}
                />
                <View style={styles.itemListText}>
                  <Text>Camisa roja a cuadros referencia 003 Levis</Text>
                  <Text style={styles.textQuantity}>$ 5000 X 8 unidades </Text>
                </View>
                <View style={styles.itemListPrice}>
                  <Text style={styles.priceProduct}>$ 100.000</Text>
                  <Text style={styles.nameProduct}>Total compra</Text>
                </View>
              </View>
            </View>
            <View style={styles.separator} />

            <View style={styles.itemList}>
              <View style={styles.infoProduct}>
                <Image
                  style={styles.imageProduct}
                  source={require('../../src/assets/camiseta-nueva.png')}
                />
                <View style={styles.itemListText}>
                  <Text>Camiseta deportiva</Text>
                  <Text style={styles.textQuantity}>$ 5000 X 8 unidades </Text>
                </View>
                <View style={styles.itemListPrice}>
                  <Text style={styles.priceProduct}>$ 100.000</Text>
                  <Text style={styles.nameProduct}>Total compra</Text>
                </View>
              </View>
            </View>
          </View>

          <View style={styles.contentTotal}>
            <View style={styles.sectionTotal}>
              <Text style={styles.textTotal}>Total compras </Text>
              <Text style={styles.priceTotal}>$ 2.450.000</Text>
            </View>
          </View>
        </ScrollView>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'column'
  },
  header: {
    backgroundColor: 'white',
    height: 60,
    paddingHorizontal: 20,
    alignItems: 'flex-start',
    justifyContent: 'center'
  },
  productDetails: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    width: '100%',
    padding: 20,
    paddingTop: 5
  },
  salesDetails: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    width: '100%',
    padding: 20,
    paddingTop: 5,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  costumerDetails: {
    flex: 1,
    backgroundColor: '#F1F1F2',
    width: '100%',
    padding: 20,
    marginBottom: 15,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  detailsLeft: {
    marginRight: 15,
    marginTop: 25,

    alignItems: 'flex-end'
  },
  sumSales: {
    marginTop: 10,
    alignItems: 'center',
    flexDirection: 'row'
  },
  contentTotal: {
    width: '100%',
    height: 50,
    backgroundColor: '#303456',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderColor: '#E5E5E5',
    paddingHorizontal: 20,
    paddingVertical: 15
  },
  priceTotal: {
    fontSize: 22,
    color: 'white'
  },
  textTotal: {
    color: 'white',
    fontSize: 14
  },
  itemList: {
    width: '95%',
    flexDirection: 'column',
    marginVertical: 15
  },
  infoProduct: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    width: '100%'
  },
  itemListText: {
    marginLeft: 8,
    justifyContent: 'center',
    width: '53%'
  },
  itemListPrice: {
    marginLeft: 8,
    justifyContent: 'center',
    width: '33%'
  },
  nameProduct: {
    fontWeight: '400',
    color: '#7f8c8d',
    fontSize: 13
  },
  textQuantity: {
    fontWeight: '400',
    color: '#7f8c8d',
    fontSize: 13,
    color: '#4834d4'
  },
  textPhotoProfile: {
    marginTop: 5,
    fontWeight: '400',
    fontSize: 12,
    color: '#4834d4'
  },
  imageProfile: {
    width: 70,
    height: 70,
    borderRadius: 15
  },
  imageProduct: {
    width: 60,
    height: 60,
    borderRadius: 6,
    marginRight: 5
  },
  title: {
    fontSize: 20,
    color: 'black',
    fontWeight: '600',
    marginBottom: 10
  },
  separator: {
    backgroundColor: '#ecf0f1',
    height: 1,
    width: '100%',
    marginVertical: 5
  },
  sectionTotal: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between'
  }
});
