import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import { Badge } from "native-base";
import Icon from "react-native-vector-icons/Feather";
import { Colors } from "../../utils/const";
import { connect } from "react-redux";

class NavBar extends React.Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.tabBar}>
        <TouchableOpacity
          style={styles.tabItem}
          onPress={() => navigate("Dash")}
        >
          <Image source={require("../../src/assets/tabBar/home.png")} />
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.tabItem}
          onPress={() => navigate("ListProducts")}
        >
          <Image source={require("../../src/assets/tabBar/stats2.png")} />
        </TouchableOpacity>

        <View style={styles.contentTabCart}>
          <TouchableOpacity
            style={styles.tabCart}
            onPress={() => navigate("NewSaleGeneralDetails")}
          >
            <Icon name="shopping-cart" style={styles.tabCartIcon} />

            {this.props.cartItems.length > 0 ? (
              <Badge
                style={{
                  width: 21,
                  height: 21,
                  position: "absolute",
                  backgroundColor: "#ff585a",
                  marginLeft: 35,
                  marginTop: 50
                }}
              >
                <Text style={{ color: "#fff" }}>
                  {this.props.cartItems.length}
                </Text>
              </Badge>
            ) : (
              <View />
            )}
          </TouchableOpacity>
        </View>

        <TouchableOpacity
          style={styles.tabItem}
          onPress={() => navigate("Sales")}
        >
          <Image source={require("../../src/assets/tabBar/history2.png")} />
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.tabItem}
          onPress={() => navigate("ListCostumers")}
        >
          <Image source={require("../../src/assets/tabBar/account2.png")} />
        </TouchableOpacity>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    cartItems: state.cartItems
  };
};
const mapDispatchToProps = dispatch => {
  return {
    addItemToCart: () => dispatch({ type: "ADD_TO_CART", payload: "hola" })
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NavBar);

const styles = StyleSheet.create({
  tabBar: {
    height: 55,
    backgroundColor: "#ffffff",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    borderColor: "#ffffff",
    borderTopWidth: 0.5,
    paddingHorizontal: 15,
    elevation: 12
  },
  tabItem: {
    alignItems: "center"
  },
  tabIcon: {
    color: "#90a4ae",
    fontSize: 22,
    fontWeight: "100"
  },
  tabTitle: {
    marginTop: 3,
    fontSize: 13,
    color: "#90a4ae"
  },
  tabIconSelected: {
    color: Colors.first,
    fontSize: 21,
    fontWeight: "900"
  },
  tabTitleSelected: {
    marginTop: 3,
    fontSize: 12,
    color: Colors.first,
    fontWeight: "600"
  },
  contentTabCart: {
    zIndex: 9999,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
    height: 55,
    width: 55,
    marginTop: -25
  },
  tabCart: {
    zIndex: 9999,
    backgroundColor: "#7B6CF6",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
    height: 46,
    width: 46,
    elevation: 5
  },
  tabCamera: {
    zIndex: 9999,
    backgroundColor: Colors.verde_aguamarina_claro,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
    height: 55,
    width: 55,
    elevation: 6,
    marginTop: -35
  },
  tabCartIcon: {
    color: "white",
    fontSize: 20,
    fontWeight: "100"
  },
  tabCartTitle: {
    marginTop: 3,
    fontSize: 10,
    color: "#90a4ae"
  }
});
