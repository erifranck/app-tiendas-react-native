import React from "react";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity
} from "react-native";
import { Colors } from "../../utils/const";
import Icon from "react-native-vector-icons/Feather";

const NavBar = ({ navigation, user, store }) => {
  return (
    <SafeAreaView style={styles.navBar}>
      <View style={styles.profileContent}>
        {user.id_facebook ? (
          <Image
            onPress={() => navigation.navigate("Settings")}
            style={styles.imageProfile}
            source={{
              uri: `https://graph.facebook.com/${
                user.id_facebook
              }/picture?type=large&width=520&height=520`
            }}
          />
        ) : (
          <Image
            onPress={() => navigation.navigate("Settings")}
            style={styles.imageProfile}
            source={{ uri: `https://api2.komercia.co/users/${user.foto}` }}
          />
        )}
        <View>
          <Text style={styles.storeName}>{store.nombre}</Text>
          <Text style={styles.userName}>Hola, {user.nombre}</Text>
        </View>
      </View>

      <TouchableOpacity
        onPress={() => navigation.navigate("Settings")}
        style={{
          alignItems: "center",
          justifyContent: "center",
          alignContent: "center",
          height: 55
        }}
      >
        <Icon
          name="settings"
          size={20}
          color={Colors.first}
          style={styles.iconSettings}
        />
      </TouchableOpacity>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  navBar: {
    height: 25,
    flexDirection: "row",
    alignContent: "center",
    alignItems: "center",
    marginBottom: 10,
    width: "100%",
    justifyContent: "space-between",

    marginBottom: 40,
    marginTop: 35
  },
  profileContent: {
    flexDirection: "row",

    marginLeft: 15,
    alignItems: "center",
    width: "80%"
  },
  navBar_title: {
    marginLeft: 3,
    paddingTop: 4,
    fontWeight: "800",
    fontSize: 19,
    color: "#4c4c4c"
  },
  imageProfile: {
    width: 45,
    height: 45,
    borderRadius: 25,
    marginTop: 0
  },
  storeName: {
    color: "#4c4c4c",
    fontSize: 18,
    fontWeight: "400",
    marginLeft: 14,
    fontFamily: "WorkSans-Medium"
  },
  userName: {
    color: Colors.first,
    fontSize: 13,
    fontWeight: "300",
    marginLeft: 14,
    fontFamily: "WorkSans-ExtraBold"
  },
  iconSettings: {
    width: 30,
    height: 30,
    marginRight: 18,
    borderRadius: 15
  }
});

export default NavBar;
