import React from "react";
import {
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableOpacity
} from "react-native";
import { Colors } from "../../utils/const";

const ButtonPrincipal = ({ navigation, link, label }) => (
  <TouchableOpacity
    style={styles.container}
    onPress={() => navigation.navigate(link)}
  >
    <Text style={styles.buttonText}>{label}</Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  container: {
    height: 48,
    width: "90%",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 4,
    backgroundColor: Colors.first,
    elevation: 1,
    margin: 10
  },
  buttonText: {
    color: "white",
    fontSize: 14
  }
});

export default ButtonPrincipal;
