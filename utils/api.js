import React from "react";
import axios from "axios";
import { AsyncStorage } from "react-native";

const BASE_API = "https://api2.komercia.co/";
const CLIENT_SECRET = "S3kE1jYcd6hFWcu0jIOm3cRFMOnjjmtmtfoYdra1";
const CLIENT_ID = "2";
let ACCESS_TOKEN = "vacio";

class Api {
  _storeData = async token => {
    try {
      await AsyncStorage.setItem("userToken", token);
    } catch (error) {}
  };

  _retrieveData = async () => {
    try {
      let token = await AsyncStorage.getItem("userToken");
      ACCESS_TOKEN = token;
    } catch (error) {
      console.warn(error);
    }
  };

  //Recuperar datos de la tienda
  _retrieveDataIdStore = async () => {
    let idStoreData = this.getStore();
    return idStoreData;
  };

  postLogin(email, password) {
    return new Promise((resolve, reject) => {
      axios
        .post(
          `${BASE_API}oauth/token`,
          {
            grant_type: "password",
            client_id: CLIENT_ID,
            client_secret: CLIENT_SECRET,
            password: password,
            username: email
          },
          {
            headers: {
              "Content-Type": "application/json"
            }
          }
        )
        .then(response => {
          this._storeData(response.data.access_token);
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  }
  //Crear nueva tienda y usuario

  createStore(nameStore, nameUser, email, phone, password) {
    return new Promise((resolve, reject) => {
      axios
        .post(
          `${BASE_API}api/tienda/registrar`,
          {
            nombre_tienda: nameStore,
            nombre: nameUser,
            email: email,
            celular: phone,
            password: password
          },
          {
            headers: {
              "Content-Type": "application/json"
            }
          }
        )
        .then(response => {
          this._storeData(response.data.access_token);
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  // Información del usuario

  async getUser() {
    await this._retrieveData();
    const user = await axios
      .get(`${BASE_API}api/user`, {
        headers: {
          Authorization: `Bearer ${ACCESS_TOKEN}`
        }
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => error);
    return user;
  }

  // Información de la tienda

  async getStore() {
    await this._retrieveData();
    const store = await axios
      .get(`${BASE_API}api/admin/tienda`, {
        headers: {
          Authorization: `Bearer ${ACCESS_TOKEN}`
        }
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => error);
    return store;
  }

  // /api/admin/tienda
  // https://api2.komercia.co/logos/1-yVW0244.png

  async getInventory() {
    await this._retrieveData();
    const products = await axios
      .get(`${BASE_API}api/admin/productos`, {
        headers: {
          Authorization: `Bearer ${ACCESS_TOKEN}`
        }
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => error);
    return products;
  }

  async getProductDetails(idProduct) {
    await this._retrieveData();
    const products = await axios
      .get(`${BASE_API}/api/admin/productos/${idProduct}`, {
        headers: {
          Authorization: `Bearer ${ACCESS_TOKEN}`
        }
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => error);
    return products;
  }

  async getProductBarCode(barcode) {
    await this._retrieveData();
    const products = await axios
      .get(`${BASE_API}/api/admin/productos/codigo/${barcode}`, {
        headers: {
          Authorization: `Bearer ${ACCESS_TOKEN}`
        }
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => error);
    return products;
  }

  // TO-DO Create new product

  saveNewProduct(
    nombre,
    foto_cloudinary,
    id_foto,
    codigo_barras,
    descripcion,
    precio,
    unidades
  ) {
    return new Promise((resolve, reject) => {
      axios
        .post(
          `${BASE_API}api/admin/productos`,
          {
            nombre: nombre,
            foto_cloudinary: foto_cloudinary,
            id_foto: id_foto,
            codigo_barras: codigo_barras,
            descripcion: descripcion,
            precio: precio,
            inventario: unidades,
            categoria_producto: 0,
            subcategoria: 0,
            activo: 1
          },
          {
            headers: {
              Authorization: `Bearer ${ACCESS_TOKEN}`
            }
          }
        )
        .then(response => {
          resolve(response);
        })
        .catch(errores => {
          console.warn("error en los datos");
          reject(errores);
        });
    });
  }

  // TO-DO Update product

  // TO-DO Delete Product

  // TO-DO show all sales

  async getSales() {
    await this._retrieveData();
    const sales = await axios
      .get(`${BASE_API}api/admin/ventas/ordenes`, {
        headers: {
          Authorization: `Bearer ${ACCESS_TOKEN}`
        }
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => error);
    return sales;
  }

  // TO-DO show sale details

  async getSaleDetails(idOrden) {
    await this._retrieveData();
    const sales = await axios
      .get(`${BASE_API}api/admin/ventas/ordenes/${idOrden}`, {
        headers: {
          Authorization: `Bearer ${ACCESS_TOKEN}`
        }
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => error);
    return sales;
  }

  //TO-DO  Update Info Store

  //TO-DO Create new costumer

  //TO-Do Get all costumers

  async getCostumers() {
    await this._retrieveData();
    const clients = await axios
      .get(`${BASE_API}/api/admin/clientes/listado`, {
        headers: {
          Authorization: `Bearer ${ACCESS_TOKEN}`
        }
      })
      .then(response => {
        return response.data.data;
      })
      .catch(error => error);
    return clients;
  }
}

export default new Api();
