import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Badge } from "native-base";

const styles = StyleSheet.create({
  buttonLogin: {
    width: 300,
    backgroundColor: "#f14b5a",
    height: 45,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginBottom: 8
  },
  alignButton: {
    flexDirection: "row",
    alignItems: "center"
  },
  buttonText: {
    color: "#fff",
    textAlign: "center",
    paddingLeft: 10,
    paddingRight: 10,
    fontSize: 16,
    fontWeight: "900"
  }
});

export const Bold = props => (
  <Text style={{ fontWeight: "bold" }}>{props.children}</Text>
);

export const Colors = {
  first: "#6F23FF",
  second: "#9e2523",
  third: "#f5aa3a",
  fourth: "#db5d2a",
  red: "#ff585a",
  another: "#12cc6f",
  morado_oscuro: "#1c2260",
  morado_claro: "#5c6af6",
  morado_claro_usado_en_alertas: "#4a59f5",
  verde_nuevo: "#57ad2a",
  verde_aguamarina_oscuro: "#0F9380",
  verde_aguamarina_claro: "#00c894",
  rojito_medio_morado: "#f14b5a",
  badge: "#0000ff"
};

export const Header = () => {
  <Header style={{ backgroundColor: "#2f3542" }}>
    <Left>
      <Button transparent onPress={() => navigate("Dash")}>
        <Icon name="arrow-left" color="white" size={20} />
      </Button>
    </Left>
    <Body>
      <Title>Inventario</Title>
    </Body>
    <Right>
      <TouchableOpacity
        style={styles.cancelButton}
        onPress={() => navigate("NewSaleBarcode")}
      />
    </Right>
  </Header>;
};

// 0.  Sin pagar
// 1.  Pagada
// 2.  Rechazada (Vendedor)
// 3.  Cancelado (Comprador)
// 4.  Despachado
// 5.  Recibido
// 6.  Finalizado y entregado
// 7.  Finalizado
// 8.  Cotizacion

export const SalesState = props => {
  let status = "";
  let color = "blue";
  let sale = props;
  switch (sale) {
    case "0":
      status = "Sin pagar";
      color = "#ffa801";
      break;
    case "1":
      status = "Pagada";
      color = "#6ab04c";
      break;
    case "2":
      status = "Rechazada";
      color = "#eb4d4b";
      break;
    case "3":
      status = "Cancelado";
      color = "#eb4d4b";
      break;
    case "4":
      status = "Despachado";
      color = "#4834d4";
      break;
    case "5":
      status = "Recibido";
      color = "#30336b";
      break;
    case "6":
      status = "Entregado";
      color = "#6ab04c";
      break;
    case "7":
      status = "Finalizado";
      color = "#00b894";
      break;
    case "8":
      status = "Cotización";
      color = "#706fd3";
      break;
  }
  return (
    <View>
      <Badge style={{ backgroundColor: color }}>
        <Text style={{ color: "white", padding: 3 }}>{status}</Text>
      </Badge>
    </View>
  );
};

export const formatCurrency = n => {
  if (n) {
    return parseInt(n)
      .toFixed()
      .replace(/(\d)(?=(\d{3})+(,|$))/g, "$1,");
  } else {
    return 0;
  }
};
