// action types
export const ADD_TO_CAR = "ADD_TO_CAR";
export const REMOVE_FROM_CAR = "REMOVE_FROM_CAR";
export const RESET_CAR = "RESET_CAR";

// action creators
export const addToCar = (product) => ({
  type: ADD_TO_CAR,
  payload: product
});

export const removeFromCar = (id) => ({
  type: REMOVE_FROM_CAR,
  id: id
});

export const resetCar = () => ({
  type: RESET_CAR,
});
// reducer
const cartItems = (state = [], action) => {
  switch (action.type) {
    case ADD_TO_CAR:
      return [...cart, action.payload];
    case REMOVE_FROM_CAR:
      return state.filter(cartItem => cartItem.id !== action.id);
    case RESET_CAR:
      return [];
    default:
      return state;
  }

  return cart;
};

export default cartItems;
